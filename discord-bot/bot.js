var Discord = require('discord.js');
var auth = require('./auth.json');
var fs = require('fs');

var config = JSON.parse(fs.readFileSync('./Storage/conf.json', 'utf8'));
var userdata = JSON.parse(fs.readFileSync('./Storage/user_stats.json', 'utf8'));
var userpoints = JSON.parse(fs.readFileSync('./Storage/points.json', 'utf8'));

const prefix = "!";

var activeEventChannel = null;
var activeEventText = null
var activeUserID = null;

// Initialize Discord Bot
const bot = new Discord.Client({
   disableEveryone: true
});

bot.on("ready", async () => {
    console.log(`Bot ${bot.user.username} is ready !`)
});

bot.on('voiceStateUpdate', (oldmember, newmember)=>{
    console.log(`Voice State Update`);
    if( newmember != null && 
        newmember.voiceChannelID != null && 
        activeEventChannel != null && 
        activeEventChannel.id === newmember.voiceChannelID){
        
        activeUserID = newmember.client.user.id;
        console.log(`Voice State Update`);
        activeEventText = server.createChannel("Random Encounter - Fight", "text",);
        activeEventText.send(`Welcome to your doom ${newmember.user.username} !`);
    }
})

bot.on("message", async message => {
    
    // SAFETY
    // if author is a bot don't do anything
    if(message.author.bot) {
        return;
    }

    // if this is a direct message don't do anything
    if(message.channel.type === "dm") {
        return;
    }

    // STATISTICS
    // si il est pas dans la base on l'ajoute
    if(!userdata[message.author.id]){
        userdata[message.author.id] = {
            messageSent: 0,
            points: 0
        }
    }

    // incrémentation du compteur de messages
    userdata[message.author.id].messageSent++;

    if(activeEventText != null && message.channel.id === activeEventText.id){
        treatFightCommand(message);
    }
    // COMMANDS
    // if not command we don't care
    if(message.content.startsWith(prefix)) {
        treatCommande(message);
    } else{
        SpawnAttempt(message);
    }

    
    // écriture du fichier sur le disque
    fs.writeFile('./Storage/user_stats.json', JSON.stringify(userdata), (err) => {
        if(err) console.log(err);
    });
});

function treatFightCommand(message){
    if(activeUserID != null &&
       message.author.id === activeUserID ) {
        message.channel.send("Nooooooooooooo !");
        activeEventChannel.delete();
        activeEventText.delete();
        
        
        activeEventChannel = null;
        activeEventText = null
        activeUserID = null;

        userdata[message.author.id].points++;
    }
}

function treatCommande(message){
    let messageArray = message.content.split(" ");  // split command 
    let command = messageArray[0];                  // read first word as command
    let args = messageArray.slice(1)                // remove command so we only keep arguments

    if(command === `${prefix}ping`){

        message.channel.send("Seriously ?")
    }
    else if(command === `${prefix}profile`) {
        let embed = new Discord.RichEmbed()
            .setAuthor(message.author.username)
            .setDescription("An awsome author !")
            .setColor("#9B59B6")
            .addField("Full Username", `${message.author.username}#${message.author.discriminator}`)
            .addField("Member since",message.author.createdAt)
            .addField("Messages sent", userdata[message.author.id].messageSent)
            .addField("Points :", userdata[message.author.id].points);
        message.channel.send(embed)
    }
}



function SpawnAttempt(textmessage){

    // par sécurité
    if (!textmessage.channel.type === "text") return;

    // nombre random
    let number = getRandomInt(100);
    console.log(`Random nbr : ${number}/${config["spawnRate"]}`);

    // si évènement il y a
    if((activeEventChannel === null) && number < config["spawnRate"]){
        console.log(`Event Occured !`);

        textmessage.channel.send("New Random Event !");
        let server = textmessage.guild;

        activeEventChannel = server.createChannel("Random Encounter", "voice");

        console.log(`New Channel created !`);
    }
}

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

bot.login(auth.token);