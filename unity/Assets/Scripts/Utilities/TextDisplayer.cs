﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextDisplayer : MonoBehaviour
{
    [Header("TextDisplayer - Must set")]
    [SerializeField] private bool hasLimitedLifeTime;
    [SerializeField] private float lifeTime;

    // Start is called before the first frame update
    void Start()
    {
        if(hasLimitedLifeTime) {
            Destroy(gameObject, lifeTime);
        } 
    }

    public float GetTime() {
        return lifeTime;
    }

}
