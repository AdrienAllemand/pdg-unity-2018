﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestEncryption : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        string str = "Hello World !";

        string encrypted = Utilities.Encrypt(str);

        string decrypted = Utilities.Decrypt(encrypted);

        Debug.Log(str + " | " + encrypted + " | " + decrypted);
    }
    
}
