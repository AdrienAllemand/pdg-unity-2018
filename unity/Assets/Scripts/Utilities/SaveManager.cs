﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveManager : MonoBehaviour
{
    public static SaveManager Instance { set; get; }
    public SaveState state;
    private string _saveKey = "save";

    private void Awake() {
        DontDestroyOnLoad(gameObject);
        Instance = this;
        Load();
    }

    public void Save() {
        PlayerPrefs.SetString(_saveKey, Utilities.Encrypt(Utilities.Serialize<SaveState>(state)));
    }

    public void Load() {
        if (PlayerPrefs.HasKey(_saveKey)) {

            Debug.Log("Save state found, loading it");
            state = Utilities.Deserialize<SaveState>(Utilities.Decrypt(PlayerPrefs.GetString(_saveKey)));
        }
        else {
            Debug.Log("No save state found, creating a new one");
            state = new SaveState();
            Save();
        }
    }
}
