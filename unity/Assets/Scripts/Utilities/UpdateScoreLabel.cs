﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TextCore;

public class UpdateScoreLabel : MonoBehaviour
{
    public void OnEnable() {

        SaveManager.Instance.Load();
        SaveState state = SaveManager.Instance.state;

        GetComponent<TMPro.TextMeshProUGUI>().text = "HighScore : " + state._highScore.ToString();
    }
}
