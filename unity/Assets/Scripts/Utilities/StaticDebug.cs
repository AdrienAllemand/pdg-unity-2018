﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StaticDebug : MonoBehaviour
{
    [SerializeField] private  Text text;
    private static Text staticText;

    private void Awake() {
        staticText = text;
    }

    public static void Log(string message) {
        if(staticText != null)
            staticText.text = message;
    }
}
