﻿using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;

public static class Utilities
{
    private static string passphrase = "The secret key of our game is 42!";

    /// <summary>
    /// T to XML string
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="toSerialize"></param>
    /// <returns></returns>
    public static string Serialize<T>(this T toSerialize) {
        XmlSerializer xml = new XmlSerializer(typeof(T));
        StringWriter writer = new StringWriter();
        xml.Serialize(writer, toSerialize);

        return writer.ToString();
    }

    /// <summary>
    /// XML string to T
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="toDeserialize"></param>
    /// <returns></returns>
    public static T Deserialize<T>(this string toDeserialize) {
        XmlSerializer xml = new XmlSerializer(typeof(T));
        StringReader reader = new StringReader(toDeserialize);

        return (T)xml.Deserialize(reader);
    }

    /// <summary>
    /// Give it a string it will return it encrypted
    /// </summary>
    /// <param name="toEncryptString"></param>
    /// <returns></returns>
    public static string Encrypt(string toEncryptString) {
        string enctyptedString = "";

        byte[] toEnctyptArray = UTF8Encoding.UTF8.GetBytes(toEncryptString);

        using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider()) {
            byte[] key = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(passphrase));
            using (TripleDESCryptoServiceProvider tripleDes = new TripleDESCryptoServiceProvider() {
                Key = key, Mode = CipherMode.ECB, Padding = PaddingMode.PKCS7
            }) {
                ICryptoTransform cTransform = tripleDes.CreateEncryptor();
                //encrypt the specified region of code to a byte array
                byte[] encryptedArray = cTransform.TransformFinalBlock(toEnctyptArray, 0, toEnctyptArray.Length);

                // release ressources held by tripleDes
                tripleDes.Clear();

                // convert to string
                enctyptedString = System.Convert.ToBase64String(encryptedArray, 0, encryptedArray.Length);
            }
        }
            return enctyptedString;
    }

    /// <summary>
    /// Give it an encrypted string it will return it decrypted
    /// </summary>
    /// <param name="toDecryptString"></param>
    /// <returns></returns>
    public static string Decrypt(string toDecryptString) {
        string decryptedString = "";

        byte[] toDecryptArray = System.Convert.FromBase64String(toDecryptString);

        using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider()) {
            byte[] key = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(passphrase));

            using (TripleDESCryptoServiceProvider tripleDes = new TripleDESCryptoServiceProvider() {
                Key = key, Mode = CipherMode.ECB, Padding = PaddingMode.PKCS7
            }) {
                ICryptoTransform cTransform = tripleDes.CreateDecryptor();

                //encrypt the specified region of code to a byte array
                byte[] decryptedArray = cTransform.TransformFinalBlock(toDecryptArray, 0, toDecryptArray.Length);

                // release ressources held by tripleDes
                tripleDes.Clear();

                // convert to string
                decryptedString = UTF8Encoding.UTF8.GetString(decryptedArray);
            }
        }

            return decryptedString;
    }
}
