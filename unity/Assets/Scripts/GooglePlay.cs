﻿using System.Collections;
using System.Collections.Generic;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GooglePlay : MonoBehaviour
{
    private Text authStatus;
    private GameObject achievements;
    private GameObject score;

    // Start is called before the first frame update
    void Start()
    {
        authStatus = GameObject.Find("authStatus").GetComponent<Text>();
        achievements = GameObject.Find("AchievementsIcon");
        score = GameObject.Find("ScoresIcon");
        
        // Try silent sign-in (second parameter is isSilent)
        PlayGamesPlatform.Instance.Authenticate(SignInCallback, true);
    }

    // Update is called once per frame
    void Update()
    {
        achievements.SetActive(Social.localUser.authenticated);
        score.SetActive(Social.localUser.authenticated);
    }

    public void ShowAchievements()
    {
        if (PlayGamesPlatform.Instance.localUser.authenticated)
        {
            Social.ShowAchievementsUI();
        }
        else
        {
            Debug.Log("Cannot show Achievements, not logged in");
        }
    }

    public void ShowLeaderboards()
    {
        if (PlayGamesPlatform.Instance.localUser.authenticated)
        {
            Social.ShowLeaderboardUI();
        }
        else
        {
            Debug.Log("Cannot show leaderboard: not authenticated");
        }
    }

    public void SignIn()
    {
        if (!PlayGamesPlatform.Instance.localUser.authenticated)
        {
            // Sign in with Play Game Services, showing the consent dialog
            // by setting the second parameter to isSilent=false.
            PlayGamesPlatform.Instance.Authenticate(SignInCallback, false);
        }
        else
        {
            // Sign out of play games
            PlayGamesPlatform.Instance.SignOut();
            authStatus.text = "You Signed Out";
        }
    }

    public void SignInCallback(bool success)
    {
        if (success)
        {
            Debug.Log("Signed in!");

            // Show the user's name
            authStatus.text = "Signed in as: " + Social.localUser.userName;
            PlayGamesPlatform.Instance.UnlockAchievement(GPGSIds.achievement_dawn_of_a_new_age, (bool s) => { Debug.Log("Welcome Unlock: " + s);});
        }
        else
        {
            Debug.Log("Sign-in failed...");

            // Show failure message
            authStatus.text = "Signed Out";
        }
    }
}
