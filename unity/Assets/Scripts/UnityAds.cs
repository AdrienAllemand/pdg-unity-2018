﻿using System.Collections;
using UnityEngine;
using UnityEngine.Monetization;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UnityAds : MonoBehaviour
{
    public string placementId = "rewardedVideo";
    string gameId = "2969827";
    bool testMode = true;


    public void Start()
    {
        if (Monetization.isSupported)
        {
            Monetization.Initialize(gameId, true);
        }
    }

    public void ShowAd()
    {
        ShowAdCallbacks options = new ShowAdCallbacks();
        options.finishCallback = HandleShowResult;
        ShowAdPlacementContent ad = Monetization.GetPlacementContent(placementId) as ShowAdPlacementContent;
        ad.Show(options);
    }

    public void HandleShowResult(ShowResult result)
    {
        if (result == ShowResult.Finished)
        {
            FindObjectOfType<Level>().Continue();
        }
        else if (result == ShowResult.Skipped)
        {
            Debug.LogWarning("The player skipped the video - DO NOT REWARD!");
        }
        else if (result == ShowResult.Failed)
        {
            Debug.LogError("Video failed to show");
        }
    }
}
