﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public Sprite SOUNDON;
    public Sprite SOUNDOFF;
    public GameObject tutorialPanel;

    private bool sound = true;
    private Image soundLogo;

    public void Start() {
        Screen.orientation = ScreenOrientation.LandscapeLeft;
        soundLogo = GameObject.Find("SoundLogo").GetComponent<Image>();
        sound = !AudioListener.pause;
        updateSoundIcon();
    }

    public void LoadScene(string s) {
        SceneManager.LoadScene(s, LoadSceneMode.Single);
    }

    public void ShowTutorial(bool b) {
        tutorialPanel.SetActive(b);
    }

    public void ToggleSound()
    {
        sound = !sound;
        AudioListener.pause = !sound;
        updateSoundIcon();
    }

    private void updateSoundIcon() {
        soundLogo.sprite = sound ? SOUNDON : SOUNDOFF;
    }

    public void ExitGame() {
        if (Application.platform == RuntimePlatform.Android) {
            AndroidJavaObject activity = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
            activity.Call<bool>("moveTaskToBack", true);
        } else {
            Application.Quit();
        }
    }
}
