﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu( fileName ="new obstaclesData", menuName ="ObstaclesData")]
public class ObstaclesData : ScriptableObject
{
    public List<GameObject> obstaclePrefabs = new List<GameObject>();
    [Tooltip("percent of chance for each obstaclePrefab to pop")]
    public List<int> appearanceChances = new List<int>();
    public GameObject boostPrefab;
    public float minDistanceBetweenObstacles;
    public float minBonusDistanceBetweenObstacles;
    public float maxBonusDistanceBetweenObstacles;
    public float initialXPosition;
    public float maxDistanceBehindPlayer;
    public float difficultyCoefficient;
    [Tooltip("Number of obstacles that are instanciated at once in the level")]
    public int numberOfCurrentObstacles;
}
