﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="new EnemiesData", menuName ="EnemiesData")]
public class EnemiesData : ScriptableObject
{
    public List<GameObject> motionlessEnemyPrefabs = new List<GameObject>();
    public List<int> appearanceChanceMotionlessEnemy = new List<int>();
    public List<GameObject> flyingEnemyPrefab = new List<GameObject>();
    public List<int> appearanceChangeFlyingEnemy = new List<int>();
    [Tooltip("Number of enemies that are instanciated at once in the level")]
    public int numberOfCurrentEnemies;
    public float distanceBetweenEnemies;
    public float initialXPosition;
    public float maxDistanceToNextFlyingEnemy;
    public float minDistanceToNextFlyingEnemy;
}
