﻿using System.Collections;
using System.Collections.Generic;
using GooglePlayGames;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Level : MonoBehaviour
{

    [Header("Level - Must set")]
    [SerializeField] private GameObject _playerPrefab;
    [SerializeField] private GameObject _followingEntityPrefab;
    //[SerializeField] private Transform _playerSpawner;
    //[SerializeField] private Transform _entitySpawner;
    /*[SerializeField] private PlayerSmoothFollower _camera;
    [SerializeField] private PlayerFollower _floor;
    [SerializeField] private ObstacleSpawner _obstacleSpawner;
    [SerializeField] private EnemySpawner _enemySpawner;
    [SerializeField] private SwipeInputs _swipeInputs;*/

    [Header("Prefabs for level instantiation (meta prefabs)")]
    [SerializeField] private GameObject _enemiesGeneratorPrefab;
    [SerializeField] private GameObject _obstacleGeneratorPrefab;
    [SerializeField] private GameObject _groundPrefab;
    [SerializeField] private GameObject _playerSpawnerPrefab;
    [SerializeField] private GameObject _entitySpawnerPrefab;
    [SerializeField] private GameObject _SwipeDetectorPrefab;
    [SerializeField] private GameObject _cameraHolderPrefab;
    [SerializeField] private string _sceneName;
    [SerializeField] private string _menuSceneName;


    [Header("UI and display Instances")]
    [SerializeField] private Text _displayScore;
    [SerializeField] private Text _displayDebug;
    [SerializeField] private LevelCanvas _levelCanvas;

    [Header("Devlopper functions")]
    [SerializeField] public static bool _testMode = true;

    //Game elements
    private GameObject _player;
    private GameObject _entity;
    private int _score;
    private int _distanceScore;
    private int _killScore;
    private bool _onPause;
    private bool _continue;
    private float _posPlayer;
    private const float _wheelToPlayer = 50;


    //meta instances
    private GameObject _enemiesGenerator;
    private GameObject _obstacleGenerator;
    private GameObject _ground;
    private GameObject _playerSpawner;
    private GameObject _entitySpawner;
    private GameObject _swipeDetector;
    private GameObject _cameraHolder;


    private void Awake() {
        //Instantiation of the meta instances
        _enemiesGenerator = Instantiate(_enemiesGeneratorPrefab);
        _obstacleGenerator = Instantiate(_obstacleGeneratorPrefab);
        _ground = Instantiate(_groundPrefab);
        _playerSpawner = Instantiate(_playerSpawnerPrefab);         //TODO 
        _entitySpawner = Instantiate(_entitySpawnerPrefab);
        _swipeDetector = Instantiate(_SwipeDetectorPrefab);
        _cameraHolder = Instantiate(_cameraHolderPrefab);

        // instantiation of the player
        _player = Instantiate(_playerPrefab, _playerSpawner.transform.position, _playerSpawner.transform.rotation);
        Player playerScriptReference = _player.GetComponent<Player>();               // a ref to player script
        playerScriptReference._dieEvent.AddListener(PlayerDeathEventCallback);

        // Instanciation of the Entity persuing the player
        _entity = Instantiate(_followingEntityPrefab, _entitySpawner.transform.position, _entitySpawner.transform.rotation);


        //TODO passer ça dans le Start

        // Pass the player Script reference to the entity
        _entity.GetComponent<Entity>().SetPlayer(playerScriptReference);

        // Pass the player script reference to the BuildingSpawners
        foreach (BuildingSpawner b in _cameraHolder.GetComponentsInChildren<BuildingSpawner>()) {
            b.SetPlayer(playerScriptReference);
        }

        // pass the player GameObject reference to all Meta Instances
        _cameraHolder.GetComponent<PlayerSmoothFollower>().SetPlayer(_player);
        _ground.GetComponent<PlayerFollower>().SetPlayer(_player);
        _obstacleGenerator.GetComponent<ObstacleSpawner>().SetPlayer(_player);
        _swipeDetector.GetComponent<SwipeInputs>().SetPlayer(_player);
        _enemiesGenerator.GetComponent<EnemySpawner>().SetPlayer(_player);

        // configure the swipedetector to display the debug text
        //_swipeDetector.GetComponent<SwipeInputs>().SetDisplayText(_displayDebug);

        // configure the _enemiesGenerator
        _enemiesGenerator.GetComponent<EnemySpawner>().SetLevelManager(this);
        
    }

    // Start is called before the first frame update
    void Start() {
        // Send Level Start at Firebase
        Firebase.Analytics.FirebaseAnalytics.LogEvent(Firebase.Analytics.FirebaseAnalytics.EventLevelStart);

        _onPause = false;
        _continue = false;
        _score = 0;
        _distanceScore = 0;
        _killScore = 0;
        DisplayScore();
    }

    // Update is called once per frame
    void Update()
    {
        if(_onPause == false) {
            if(_player != null) {
                _distanceScore = (int)_player.transform.position.x;
            }
            DisplayScore();
        }
    }

    public void ReloadLevel() {
        SceneManager.LoadScene(_sceneName, LoadSceneMode.Single);
    }

    public void ReloadMenuScene() {
        SceneManager.LoadScene(_menuSceneName, LoadSceneMode.Single);
    }

    private void DisplayScore() {
        _score = _distanceScore + _killScore;
        _displayScore.text = _score.ToString();
    }

    public void AddPoints(int points) {
        _killScore += points;
    }

    // called by the player when he dies
    private void PlayerDeathEventCallback() {
        // Send to firebase death (LevelEnd) and score event
        Firebase.Analytics.FirebaseAnalytics.LogEvent(Firebase.Analytics.FirebaseAnalytics.EventLevelEnd);
        Firebase.Analytics.FirebaseAnalytics.LogEvent("score", "pts", _score);

        // update and save the score
        SaveManager.Instance.Load();
        SaveState state = SaveManager.Instance.state;
        int oldScore = state._highScore;

        if(PlayGamesPlatform.Instance.IsAuthenticated())
        {
            PlayGamesPlatform pgp = PlayGamesPlatform.Instance;
            // Notify GooglePlay of new Score
            pgp.ReportScore(_score, GPGSIds.leaderboard_best_score, (bool success) => { Debug.Log("Success"); });
            // Give Achievements
            if(_score >= 500 && _score < 1000)
            {
                pgp.UnlockAchievement(GPGSIds.achievement_500, (bool success) => { Debug.Log("500m badge won"); });
            }
            if (_score >= 1000 && _score < 2000)
            {
                pgp.UnlockAchievement(GPGSIds.achievement_1000, (bool success) => { Debug.Log("500m badge won"); });
            }
            if (_score >= 2000 && _score < 5000)
            {
                pgp.UnlockAchievement(GPGSIds.achievement_2000, (bool success) => { Debug.Log("500m badge won"); });
            }
            if (_score >= 5000)
            {
                pgp.UnlockAchievement(GPGSIds.achievement_5000_really, (bool success) => { Debug.Log("500m badge won"); });
            }
        }
        

        if(oldScore < _score) {
            state._highScore = _score;
            SaveManager.Instance.Save();
        }

        // call the LevelCanvas to display the score
        _levelCanvas.DisplayGameOver(oldScore, _score);

        // désactiver les labels
        _displayDebug.gameObject.SetActive(false);
        _displayScore.gameObject.SetActive(false);

        if (!_continue)
        {
            _posPlayer = _player.transform.position.x;
            Destroy(_entity);
            Destroy(_player);
            Destroy(_swipeDetector);
            _enemiesGenerator.GetComponent<EnemySpawner>().clearAll();
            _obstacleGenerator.GetComponent<ObstacleSpawner>().DeleteAllObstacles();
            _obstacleGenerator.GetComponent<ObstacleSpawner>().RenewObstacles();
        }
    }

    

    public void Continue()
    {
        if (!_continue)
        {
            _continue = true;

            _player = Instantiate(_playerPrefab, new Vector2(_posPlayer, _playerSpawner.transform.position.y), _playerSpawner.transform.rotation);
            _swipeDetector = Instantiate(_SwipeDetectorPrefab);

            Player playerScriptReference = _player.GetComponent<Player>();               // a ref to player script
            playerScriptReference._dieEvent.AddListener(PlayerDeathEventCallback);

            // Instanciation of the Entity persuing the player
            _entity = Instantiate(_followingEntityPrefab, new Vector2(_posPlayer - _wheelToPlayer, _entitySpawner.transform.position.y), _entitySpawner.transform.rotation);
            // Pass the player Script reference to the entity
            _entity.GetComponent<Entity>().SetPlayer(playerScriptReference);

            // Pass the player script reference to the BuildingSpawners
            foreach (BuildingSpawner b in _cameraHolder.GetComponentsInChildren<BuildingSpawner>())
            {
                b.SetPlayer(playerScriptReference);
            }
             

            // pass the player GameObject reference to all Meta Instances
            _cameraHolder.GetComponent<PlayerSmoothFollower>().SetPlayer(_player);
            _ground.GetComponent<PlayerFollower>().SetPlayer(_player);
            _obstacleGenerator.GetComponent<ObstacleSpawner>().SetPlayer(_player);
            _swipeDetector.GetComponent<SwipeInputs>().SetPlayer(_player);
            _enemiesGenerator.GetComponent<EnemySpawner>().SetPlayer(_player);

            Button continue_button = GameObject.Find("ContinueButton").GetComponent<Button>();
            continue_button.interactable = false;

            // call the LevelCanvas to hide the game over
            _levelCanvas.HideGameOver();

            // activé les labels
            _displayDebug.gameObject.SetActive(false);
            _displayScore.gameObject.SetActive(true);
        }
    }
}
