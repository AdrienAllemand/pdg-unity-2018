﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelCanvas : MonoBehaviour
{

    [Header("LevelCanvas - Must set")]
    [SerializeField] private GameObject _panelGameOver;
    [SerializeField] private Text _highScoreText;
    [SerializeField] private GameObject _newHighScoreGroup;
    [SerializeField] private Text _newScoreText;

    private Level level;

    // Start is called before the first frame update
    void Start()
    {
        HideGameOver();
    }

    public void DisplayGameOver(int rank, int newScore) {

        // Update the scores
        UpdateGameOver(rank, newScore);

        // Display them on screen
        _panelGameOver.SetActive(true);
    }

    public void HideGameOver() {

        _panelGameOver.SetActive(false);
        _newHighScoreGroup.SetActive(false);
    }

    public void UpdateGameOver(int oldScore, int newScore) {
        if(oldScore < newScore) {
            _newHighScoreGroup.SetActive(true);
            _highScoreText.text = newScore.ToString();
        } else {
            _highScoreText.text = oldScore.ToString();
        }

        _newScoreText.text = newScore.ToString();
    }
}
