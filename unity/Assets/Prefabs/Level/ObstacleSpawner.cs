﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawner : MonoBehaviour {

    [Header("ObstacleSpawner - Must set")]
    [SerializeField] private ObstaclesData obstaclesData;

    private GameObject _followingEntity;
    private GameObject _player;
    private List<GameObject> _obstacles;
    private List<int> _obstaclesIndexesProportions;
    private float _xPos;
    private int _obstacleNumber;
    private bool _onPause;

    // Start is called before the first frame update
    void Start() {
        _onPause = false;
        //We keep a tracks on every obstacle (or group of obstacles) instanciated
        _obstacleNumber = 0;
        //the _obstacles list will contain every obstacle currently instantiated
        _obstacles = new List<GameObject>();

        //This list is used to select the next obstaclePrefab to instantiate with a certain amount of chance for each prefab
        _obstaclesIndexesProportions = new List<int>();

        //each prefab has an int value (appeareanceChance) describing its chances to appear in the scene. We add the position index of the prefab in the _obstacleIndexesProportions "appearanceChance" times. 
        for(int i = 0; i < obstaclesData.obstaclePrefabs.Count; ++i) {
            int chanceOfAppearance = obstaclesData.appearanceChances[i];
            for(int j = 0; j < chanceOfAppearance; ++j) {
                _obstaclesIndexesProportions.Add(i);
            }
        }

        _xPos = obstaclesData.initialXPosition;


        for (int i = 0; i < obstaclesData.numberOfCurrentObstacles; ++i) {
            InstantiateObstacle();
        }


    }

    // Update is called once per frame
    /// <summary>
    /// Every frame we check if an obstacle has to be destroyed (an obstacle is destroyed when it is behind the player at a given distance). If an obstacle is destroyed and removed from the 
    /// list of obstacles and an other one is created in front of the player
    /// </summary>
    void Update() {
        if(_onPause == false) {
            if(_player != null) {
                for(int i = 0; i < _obstacles.Count; ++i) {
                    if(_obstacles[i].transform.position.x <= _player.transform.position.x - _obstacles[i].GetComponent<Pattern>().GetLength() - obstaclesData.maxDistanceBehindPlayer) {
                        InstantiateObstacle();
                        GameObject temp = _obstacles[i];
                        _obstacles.Remove(_obstacles[i]);
                        Destroy(temp);
                    }
                }
            }
        }  
    }

    /// <summary>
    /// Instantiate an obstacle an add it to the list of obstacles currently present on the map
    /// </summary>
    /// <returns>The new obstacle</returns>
    private GameObject InstantiateObstacle() {

        Vector3 pos = new Vector3(_xPos, 0, 0);
        GameObject obstacle = null;

        // Add a boost every 3 obstacle
        if (_obstacleNumber % 3 == 0) {
            obstacle = Instantiate(obstaclesData.boostPrefab, pos, Quaternion.identity);
        } else {
            int i = Random.Range(0, _obstaclesIndexesProportions.Count);
            int obstacleIndex = _obstaclesIndexesProportions[i];
            obstacle = Instantiate(obstaclesData.obstaclePrefabs[obstacleIndex], pos, Quaternion.identity);
        }

        //add the obstacle to the list of currentrly instantiacte obstacles
        _obstacles.Add(obstacle);

        // increment the position of the following obstacle.
        float oldXPos = _xPos;
        _xPos += obstacle.GetComponent<Pattern>().GetLength() + obstaclesData.minDistanceBetweenObstacles + (Random.Range(obstaclesData.minBonusDistanceBetweenObstacles, obstaclesData.maxBonusDistanceBetweenObstacles) * (1f - Difficulty()));
        StaticDebug.Log((_xPos - oldXPos).ToString("n2") + " " + (1f - Difficulty()).ToString("n2"));
        _obstacleNumber++;
        return obstacle;
    }

    /// <summary>
    /// Set the player gameObject
    /// </summary>
    /// <param name="player"></param>
    public void SetPlayer(GameObject player) {
        _player = player;
    }

    /// <summary>
    /// Returns a coefficient between ]0,1[ based on the current distance of the obstacle
    /// </summary>
    /// <returns></returns>
    public float Difficulty() {
        return 1f - (1f / (1f + (_xPos / obstaclesData.difficultyCoefficient)));
    }

    /// <summary>
    /// Delete all obstacles currently in the scene
    /// </summary>
    public void DeleteAllObstacles() {
        foreach(GameObject o in _obstacles) {
            Destroy(o);
        }
        _obstacles.Clear();
    }

    /// <summary>
    /// Replace the obstacles in the scene in front of the player
    /// </summary>
    public void RenewObstacles() {
        _xPos = _player.transform.position.x + obstaclesData.initialXPosition;
        for(int i = 0; i < obstaclesData.numberOfCurrentObstacles; ++i) {
            InstantiateObstacle();
        }
    }

}
