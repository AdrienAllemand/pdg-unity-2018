﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [Header("EnemySpawner - Must set")]
    [SerializeField] private EnemiesData _enemiesData;

    private List<int> _indexFlyingEnemyProportion;
    private List<int> _indexMotionlessEnemyProportion;
    private Level _levelManager;
    private GameObject _player;
    private List<GameObject> _enemies;
    private float _xPos;
    private GameObject _currentFlyingEnemy;
    private bool _onPause;



    // Start is called before the first frame update
    void Start()
    {
        _onPause = false;
        _indexFlyingEnemyProportion = new List<int>();
        _indexMotionlessEnemyProportion = new List<int>();
        _enemies = new List<GameObject>();

        _xPos = _enemiesData.initialXPosition;

        populateListOfAppearanceChances(_enemiesData.motionlessEnemyPrefabs, _enemiesData.appearanceChanceMotionlessEnemy, _indexMotionlessEnemyProportion);
        populateListOfAppearanceChances(_enemiesData.flyingEnemyPrefab, _enemiesData.appearanceChangeFlyingEnemy, _indexFlyingEnemyProportion);
        
        if(_enemiesData.motionlessEnemyPrefabs.Count != 0) {
            for(int i = 0; i < _enemiesData.numberOfCurrentEnemies; ++i) {
                InstantiateEnemy();
            }
        }

        if(_enemiesData.flyingEnemyPrefab.Count != 0) {
            _currentFlyingEnemy = InstanciateFlyingEnemy();
        }
        
    }

    // Update is called once per frame
    /// <summary>
    /// Every frame, the class check if a flying enemy is instantiate, if not, it instantiate's it
    /// </summary>
    void Update()
    {
        if(_onPause == false) {
            if(_enemiesData.flyingEnemyPrefab.Count != 0) {
                if(_currentFlyingEnemy == null) {
                    _currentFlyingEnemy = InstanciateFlyingEnemy();
                }
            }
        }
    }

    /// <summary>
    /// Set the instance of the player
    /// </summary>
    /// <param name="player"></param>
    public void SetPlayer(GameObject player) {
        _player = player;
    }

    //CURRENTLY USED IN A DEPRECIATED CLASS - MUST NOT BE ERASED
    /// <summary>
    /// Destroy an enemy instance and recreate a new instance of the enemy
    /// </summary>
    /// <param name="enemyToRemove"></param>
    public void RemoveAndRenewEnemy(GameObject enemyToRemove) {
        if(_enemies.Contains(enemyToRemove)) {
            //The old enemy is removed from the list
            _enemies.Remove(enemyToRemove);
            //a new enemy is added to the scene
            InstantiateEnemy();
        }
    }

    /// <summary>
    /// Instantiate a motionless enemy .
    /// </summary>
    /// <returns></returns>
    private GameObject InstantiateEnemy() {
        //Chose the enemy to instantiate
        GameObject prefab = ChoosePrefabFromList(_enemiesData.motionlessEnemyPrefabs, _indexMotionlessEnemyProportion);
        Vector3 pos = new Vector3(_xPos, prefab.transform.position.y, 0);
        _xPos += _enemiesData.distanceBetweenEnemies;
        GameObject enemy = Instantiate(prefab, pos, Quaternion.identity);
        enemy.GetComponent<Enemy>().SetPlayer(_player);
        //Permis optional callbacks
        enemy.GetComponent<Enemy>().SetEnemySpawner(this);
        enemy.GetComponent<Enemy>().SetLevelManager(_levelManager);
        _enemies.Add(enemy);
        return enemy;
    }

    /// <summary>
    /// Instantiate a flying enemy. A flying enemy is instantiated ahead of the player
    /// </summary>
    /// <returns></returns>
    public GameObject InstanciateFlyingEnemy() {
        GameObject enemy = null;
        if(_player != null) {
            float xPos = Random.Range(_enemiesData.minDistanceToNextFlyingEnemy, _enemiesData.maxDistanceToNextFlyingEnemy);
            Vector3 position = new Vector3(_player.transform.position.x + xPos, 0, 0);
            //instantiate a flying enemy randomly chosen in the list of prefabs
            enemy = Instantiate(ChoosePrefabFromList(_enemiesData.flyingEnemyPrefab, _indexFlyingEnemyProportion), position, Quaternion.identity);
            enemy.GetComponent<Enemy>().SetPlayer(_player);
            //Permis optional callbacks
            enemy.GetComponent<Enemy>().SetEnemySpawner(this);
            enemy.GetComponent<Enemy>().SetLevelManager(_levelManager);
        }
        return enemy;
    }

    /// <summary>
    /// Set the levelManager
    /// </summary>
    /// <param name="levelManager"></param>
    public void SetLevelManager(Level levelManager) {
        _levelManager = levelManager;
    }


    /// <summary>
    /// Set the EnemiesData
    /// </summary>
    /// <param name="ed"></param>
    public void SetEnemiesData(EnemiesData ed) {
        _enemiesData = ed;
    }


    /// <summary>
    /// Fill the list of indexPrefabsProportions with the indexes of the differents prefabs depending on their given chance of appearance
    /// </summary>
    /// <param name="prefabs"></param>
    /// <param name="appearanceChances"></param>
    /// <param name="indexProportions"></param>
    private void populateListOfAppearanceChances(List<GameObject> prefabs, List<int> appearanceChances, List<int> indexProportions) {
        if(prefabs.Count != appearanceChances.Count) {
            Debug.LogError("prefabs and appearanceChances must be the same size");
        }
        for(int i = 0; i < prefabs.Count; ++i) {
            int appearanceChance = appearanceChances[i];
            for(int j = 0; j < appearanceChance; ++j) {
                indexProportions.Add(i);
            }
        }
    }

    /// <summary>
    /// Chose a Prefab from a list propotionnaly to its chances of appearance
    /// </summary>
    /// <param name="prefabs"></param>
    /// <param name="appearanceChances"></param>
    /// <returns></returns>
    private GameObject ChoosePrefabFromList(List<GameObject> prefabs, List<int> appearanceChances) {
        int i = Random.Range(0, appearanceChances.Count);
        int indexPrefab = appearanceChances[i];
        return prefabs[indexPrefab];
    }

    public void DestroyAllEnemies() {
        Destroy(_currentFlyingEnemy);
    }

    /// <summary>
    /// Delete all the Enemy create by this spawner
    /// </summary>
    public void clearAll()
    {
        if (_enemies.Count > 0)
        {
            foreach (GameObject enemy in _enemies)
            {
                Destroy(enemy);
            }
        }

        Destroy(_currentFlyingEnemy);
    }
}
