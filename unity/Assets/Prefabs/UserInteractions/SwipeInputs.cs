﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class SwipeInputs : MonoBehaviour
{

    [System.Serializable]
    public class SwipeEvent : UnityEvent<float, float> {

    }

    [Header("SwipeInputs - Must set")]
    [Tooltip("Screen devided by swipeDistanceRatio")]
    [SerializeField] private float _swipeDistanceRatio;
    [SerializeField] private LayerMask _enemyHitMask;
    [SerializeField] private GameObject _touchFeedbackPrefab;

    [Header("SwipeInputs - Optional set")]
    [SerializeField] private SwipeEvent _leftSwipe = new SwipeEvent();
    [SerializeField] private SwipeEvent _rightSwipe = new SwipeEvent();

    private Text _display;
    private float _distance;
    private GameObject _player;
    private Camera _mainCamera;

    private Vector3 _lastmouseposition;

    public struct Swipe {
        public int xStart;
        public int yStart;
        public int xFinish;
        public int yFinish;
        public float distance;
        public GameObject swipeFeedback;
    }

    // stores the current pending touches of the user
    private Dictionary<int, Swipe> activeTouches = new Dictionary<int, Swipe>();


    // Start is called before the first frame update
    void Start(){

        _lastmouseposition = Input.mousePosition;
        _mainCamera = Camera.main;
        _distance = Screen.width /_swipeDistanceRatio;
        Screen.orientation = ScreenOrientation.LandscapeLeft;
        _leftSwipe.AddListener(_player.GetComponent<Player>().ProcessSwipe);
    }

    // Update is called once per frame
    void Update() {
        
        // run trough each potential touch
        for(int i = 0; i < Input.touchCount; i++) {

            // the touch we are tracking
            Touch t = Input.GetTouch(i);

            if(t.phase == TouchPhase.Began) {
                BeginTouch(t);

            } else if(t.phase == TouchPhase.Moved) {
                MoveTouch(t);

            } else if(t.phase == TouchPhase.Ended) {
                EndTouch(t);

            } else if(t.phase == TouchPhase.Canceled) {
                CancelTouch(t);
            }
        }
        

        if (Input.GetMouseButton(0)) {
            Touch fakeTouch = new Touch();
            fakeTouch.fingerId = 10;
            fakeTouch.position = Input.mousePosition;
            fakeTouch.deltaTime = Time.deltaTime;
            fakeTouch.deltaPosition = Input.mousePosition - _lastmouseposition;
            fakeTouch.phase = (Input.GetMouseButtonDown(0) ? TouchPhase.Began :
                                (fakeTouch.deltaPosition.sqrMagnitude > 1f ? TouchPhase.Moved : TouchPhase.Stationary));
            fakeTouch.tapCount = 1;


            if (fakeTouch.phase == TouchPhase.Began) {
                BeginTouch(fakeTouch);

            } else if (fakeTouch.phase == TouchPhase.Moved) {
                MoveTouch(fakeTouch);

            } else if (fakeTouch.phase == TouchPhase.Ended) {
                EndTouch(fakeTouch);

            } else if (fakeTouch.phase == TouchPhase.Canceled) {
                CancelTouch(fakeTouch);
            }
        }
        _lastmouseposition = Input.mousePosition;
    }

    private void BeginTouch(Touch t) {

        // beginning of a swipe, we record the initial position of the touch
        Swipe s = new Swipe();
        s.swipeFeedback = Instantiate(_touchFeedbackPrefab, _mainCamera.ScreenToWorldPoint(t.position), Quaternion.identity);
        s.xStart = (int)t.position.x;
        s.yStart = (int)t.position.y;
        s.distance = 0f;

        activeTouches.Add(t.fingerId, s);
    }

    private void MoveTouch(Touch t) {
        //get the swSpe associated with the Touch
        Swipe s;
        if(activeTouches.TryGetValue(t.fingerId, out s)){
            s.distance = Mathf.Sqrt((t.position.x - s.xStart) * (t.position.x - s.xStart)
            + (t.position.y - s.yStart) * (t.position.y - s.yStart));

            Vector3 pos = _mainCamera.ScreenToWorldPoint(t.position);

            s.swipeFeedback.transform.position = pos;
            
            RaycastHit2D hit = Physics2D.Raycast(pos, t.deltaPosition, t.deltaPosition.magnitude, _enemyHitMask);
            Enemy enemy;

            if (hit.collider != null && (enemy = hit.collider.GetComponentInParent<Enemy>()) != null) {
                if (_display != null)
                {
                    _display.text = "Swiped!";
                }
                s.xFinish = (int)t.position.x;
                s.yFinish = (int)t.position.y;

                enemy.ProcessSwipe(s, hit);

                activeTouches.Remove(t.fingerId);
            }

            //if the distance 
            if (s.distance >= _distance) {
                s.xFinish = (int)t.position.x;
                s.yFinish = (int)t.position.y;
                ProcessSwipe(s);
                activeTouches.Remove(t.fingerId);
                Destroy(s.swipeFeedback, 0.5f);
            }
        }
    }

    private void EndTouch(Touch t) {
        activeTouches.Remove(t.fingerId);
        Swipe s;
        if (activeTouches.TryGetValue(t.fingerId, out s)) {
            Destroy(s.swipeFeedback, 0.5f);
        }
        Debug.Log("SwipeInputs : swipe too small !");
    }

    private void CancelTouch(Touch t) {
        Swipe s;
        if (activeTouches.TryGetValue(t.fingerId, out s)) {
            Destroy(s.swipeFeedback, 0.5f);
        }
        // purge the dictionnary
        activeTouches.Remove(t.fingerId);
        Debug.Log("SwipeInputs : touch canceled !");
    }

    private void ProcessSwipe(Swipe s) {
        string result = "";

        // determine on witch side of the screen the swipe was destined
        int xSum = s.xStart + s.xFinish;

        // now detecting one of the 8 directions (4 axis)
        Vector2 svector = new Vector2(s.xStart - s.xFinish, s.yStart - s.yFinish);

        float angle = Vector2.SignedAngle(Vector2.up, svector);

        if(xSum > 2 * _mainCamera.WorldToScreenPoint(_player.transform.position).x) {
            result += "R - ";

            // on invoque l'évènement de swipe en lui passant l'angle et la distance parcourue
            _rightSwipe.Invoke(angle, svector.magnitude);
        } else {
            result += "L - ";

            // on invoque l'évènement de swipe en lui passant l'angle et la distance parcourue
            _leftSwipe.Invoke(angle, svector.magnitude);
        }
        
        result += DefineDirection(angle);   
        if(_display != null) {
            _display.text = result;
        }
    }

    private string DefineDirection(float angle) {
        string direction = "";
        if(angle < -157.5) {
            direction = "Up";
        } else if(angle < -112.5) {
            direction = "Up-Left";
        } else if(angle < -67.5) {
            direction =  "Left";
        } else if(angle < -22.5) {
            direction = " Down-Left";
        } else if(angle < 22.5) {
            direction = " Down";
        } else if(angle < 67.5) {
            direction = " Down-Right";
        } else if(angle < 112.5) {
            direction = "Right";
        } else if(angle < 157.5) {
            direction = "Up-Right";
        } else {
            direction = "Up";
        }
        return direction;
    }

    public void SetPlayer(GameObject player) {
        _player = player;
    }

    public void SetDisplayText(Text text) {
        _display = text;
    }

}
