﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSmoothFollower : MonoBehaviour {

    [Header("PlayerSmoothFollower - Must set")]
    [SerializeField] private float _xOffset;
    [SerializeField] private float smoothSpeed;

    private GameObject _player;
    private Vector3 _cameraPosition;
    

    // Update is called once per frame
    void Update() {
        if (_player != null) {
            FollowPlayer();
        }
    }

    private void FollowPlayer() {
        Vector3 moveCamera = new Vector3(_player.transform.position.x + _xOffset, transform.position.y, transform.position.z);
        transform.position = Vector3.Lerp(transform.position, moveCamera, smoothSpeed * Time.deltaTime);
    }
    
    public void SetPlayer(GameObject player) {
        _player = player;
    }

}

