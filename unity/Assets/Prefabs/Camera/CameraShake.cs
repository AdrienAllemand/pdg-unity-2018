﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{

    public class ShakeEvent {
        float duration;
        float timeRemaining;

        ShakeTransformEventData data;

        public ShakeTransformEventData.Target target {
            get {
                return data.target;
            }
        }

        private Vector3 noiseOffset;
        public Vector3 noise;

        public ShakeEvent(ShakeTransformEventData data) {

            this.data = data;

            duration = data.duration;
            timeRemaining = duration;

            float rand = 32.0f;

            noiseOffset.x = Random.Range(0.0f, rand);
            noiseOffset.y = Random.Range(0.0f, rand);
            noiseOffset.z = Random.Range(0.0f, rand);
        }

        public void Update() {

            float deltaTime = Time.deltaTime;

            timeRemaining -= deltaTime;

            // apply frequency as movement speed in X
            float noiseOffsetDelta = deltaTime * data.frequency;

            // calculate movement in X
            noiseOffset.x += noiseOffsetDelta;
            noiseOffset.y += noiseOffsetDelta;
            noiseOffset.z += noiseOffsetDelta;

            // moves the noise in X ( and keeps them offset in Y)
            noise.x = Mathf.PerlinNoise(noiseOffset.x, 0.0f);
            noise.y = Mathf.PerlinNoise(noiseOffset.x, 1.0f);
            noise.z = Mathf.PerlinNoise(noiseOffset.x, 2.0f);

            // center noise
            noise -= Vector3.one * 0.5f;

            // apply amplitude
            noise *= data.amplitude;

            // apply curve
            float agePercent = 1.0f - (timeRemaining / duration);
            noise *= data.blendOverLifetime.Evaluate(agePercent);

        }

        public bool IsAlive() {
            return timeRemaining > 0.0f;
        }
    }

    List<ShakeEvent> shakeEvents = new List<ShakeEvent>();

    public void AddShakeEvent(ShakeTransformEventData data) {
        shakeEvents.Add(new ShakeEvent(data));
    }

    public void AddShakeEvent(float amplitude, float frequency, float duration, AnimationCurve animationCurve, ShakeTransformEventData.Target target) {

        ShakeTransformEventData data = ScriptableObject.CreateInstance<ShakeTransformEventData>();
        data.Init(amplitude, frequency, duration, animationCurve, target);

        AddShakeEvent(data);
    }

    private void LateUpdate() {

        // final offset that will be applied
        Vector3 positionOffset = Vector3.zero;
        Vector3 rotationOffset = Vector3.zero;

        for(int i = shakeEvents.Count -1; i != -1; i--) {
            ShakeEvent se = shakeEvents[i];
            se.Update();

            if(se.target == ShakeTransformEventData.Target.Position) {
                positionOffset += se.noise;
            } else {
                rotationOffset += se.noise;
            }

            if(!se.IsAlive()) {
                shakeEvents.RemoveAt(i);
            }

            transform.localPosition = positionOffset;
            transform.localEulerAngles = rotationOffset;
        }
    }

    /*
    IEnumerator Shake (float duration, float magnitude) {
        Vector3 originalPosition = transform.localPosition;

        float elapsed = 0.0f;

        while (elapsed < duration) {
            float x = Random.Range(-1f, 1f) * magnitude;
            float y = Random.Range(-1f, 1f) * magnitude;

            transform.localPosition = new Vector3(x, y, originalPosition.z);

            elapsed += Time.deltaTime;

            yield return null;
        }

        transform.localPosition = originalPosition;
    }
    */
}
