﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPlayerFollower : MonoBehaviour {

    [Header("CameraPlayerFollower - Must set")]
    [SerializeField] private GameObject player;
    [SerializeField] private float offset;

    private Vector3 cameraPosition;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        FollowPlayer();
    }

    private void FollowPlayer() {

        //The camera will follow the player in the x direction
        cameraPosition = new Vector3(player.transform.position.x + offset, transform.position.y, transform.position.z);
    
        transform.position = cameraPosition;
    }

}
