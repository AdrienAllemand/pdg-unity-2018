﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Player : MonoBehaviour
{

    /// <summary>
    /// Care, the collision is from the point of view of the player !
    /// </summary>
    public struct PlayerCollision2D {
        public Collision2D collision;
        public Player player;
    }

    [Header("NewPlayer - Must set variables")]
    [SerializeField] private float _deathAnimationLength;
    public float Speed { get { return _speed; } private set { _speed = value; } }

    /*
    [SerializeField] private float _acceleration;
    [SerializeField] private float _minSpeed;
    [SerializeField] private float _mediumSpeed;
    [SerializeField] private float _maxSpeed;
    */
    [SerializeField] private PlayerData _playerData;

    [SerializeField] private ShakeTransformEventData deathShake;
    [SerializeField] private ShakeTransformEventData jumpShake;
    [SerializeField] private ShakeTransformEventData duckShake;
    [SerializeField] private ShakeTransformEventData collisionShake;


    private UnityEvent _jumpEvent = new UnityEvent();
    private UnityEvent _duckEvent = new UnityEvent();

    private Vector3 _movementVector;
    private Animator _anim;
    private bool _isDead;
    private bool _isJumping;
    private bool _isDucking;
    private float _speed;
    private bool _canJump;
    private bool _canDuck;
    private bool _stops;

    public UnityEvent _dieEvent = new UnityEvent();

    // Start is called before the first frame update
    void Start() {
        //By default, the player can jump and duck
        _canJump = true;
        _canDuck = true;
        //By default, the player moves
        _stops = false;

        _anim = GetComponentInChildren<Animator>();
        _movementVector = new Vector3(_speed, 0, 0);
        _isDead = false;
        _isJumping = false;
        _isDucking = false;
        _speed = _playerData.mediumSpeed;
    }

    // Update is called once per frame
    void Update() {

        if(_isDead == false) {
            MoveForward();
            
            if(_stops == false) {
                // Retours vers la speed normale après un ralentissement
                if(_speed < _playerData.mediumSpeed) {
                    _speed = _speed + Time.deltaTime * _playerData.acceleration > _playerData.mediumSpeed ? _playerData.mediumSpeed : _speed + Time.deltaTime * _playerData.acceleration;
                    _anim.SetFloat("speed", _speed / _playerData.mediumSpeed);
                } 
                // Retour vers la speed normale après une accélération
                else if(_speed > _playerData.mediumSpeed) {
                    _speed = _speed - Time.deltaTime * _playerData.acceleration < _playerData.mediumSpeed ? _playerData.mediumSpeed : _speed - Time.deltaTime * _playerData.acceleration;
                    _anim.SetFloat("speed", _speed / _playerData.mediumSpeed);
                }
                else {
                    _speed = _playerData.mediumSpeed;
                }
            }
            _movementVector = new Vector3(_speed, 0, 0);
        }

        if (Level._testMode) {
            if (Input.GetKeyDown(KeyCode.W)) {
                Jump();
            }

            if (Input.GetKeyDown(KeyCode.S)) {
                Duck();
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision) {

        PlayerCollision2D pc = new PlayerCollision2D();
        pc.player = this;
        pc.collision = collision;

        collision.gameObject.SendMessageUpwards("OnPlayerCollisionEnter", pc);
    }

    public void ProcessSwipe(float angle, float distance) {

        // test d'un swipe vers le haut
        if(angle < -135.0f || angle > 135) {
            if(_canJump == true) {
                Jump();
                
            }
        }

        //if the swipe goes down
        if((angle > -45 && angle <= 45)) {
            if(_canDuck == true) {
                Duck();
            }
        }
 
    }

    private void MoveForward() {
        transform.position += _movementVector * Time.deltaTime;
    }

    private void Jump() {
        //Camera.main.GetComponentInParent<CameraShake>().AddShakeEvent(jumpShake);
        _anim.SetBool("jump", true);
        _jumpEvent.Invoke();
    }

    private void Duck() {
        //Camera.main.GetComponentInParent<CameraShake>().AddShakeEvent(duckShake);
        _anim.SetBool("duck", true);

        _duckEvent.Invoke();
    }

    public void Die() {

        Camera.main.GetComponentInParent<CameraShake>().AddShakeEvent(deathShake);

        // désactiver les colliders
        Collider2D[] cols = GetComponentsInChildren<Collider2D>();
        foreach(Collider2D col in cols) {
            col.enabled = false;
        }

        _isDead = true;
        _speed = 0;
        // Destroy(gameObject, _deathAnimationLength);
        _anim.SetTrigger("die");

        // trigger death event
        _dieEvent.Invoke();
    }

    public void Slow() {
        Debug.Log("The player is Slow!");
        _speed = _playerData.minSpeed;
    }
    
    public void SetJumpAuthorization(bool b) {
        _canJump = b;
    }

    public void Boost() {
        Debug.Log("The player is Fast!");
        _speed = _playerData.maxSpeed;
    }
   
    public void SetDuckAuthorization(bool b) {
        _canDuck = b;
    }

    public void SetStop(bool b) {
        if(b == true) {
            _speed = 0f;
        }
        _stops = b;
    }

    public void AddJumpEventListener(UnityAction listener) {
        _jumpEvent.AddListener(listener);
    }

    public void RemoveJumpEventListener(UnityAction listener) {
        _jumpEvent.RemoveListener(listener);
    }

    public void AddDuckEventListener(UnityAction listener) {
        _duckEvent.AddListener(listener);
    }

    public void RemoveDuckEventListener(UnityAction listener) {
        _duckEvent.RemoveListener(listener);
    }

    public void ChangeSpeedData(float mediumSpeed, float minSpeed, float maxSpeed) {
        _playerData.mediumSpeed = mediumSpeed;
        _playerData.minSpeed = minSpeed;
        _playerData.maxSpeed = maxSpeed;
    }

}
