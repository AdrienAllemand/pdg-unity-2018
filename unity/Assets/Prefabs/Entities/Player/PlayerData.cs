﻿using UnityEngine;

[CreateAssetMenu(fileName ="new PlayerData", menuName ="PlayerData")]
public class PlayerData : ScriptableObject
{
    public float acceleration;
    public float minSpeed;
    public float mediumSpeed;
    public float maxSpeed;
}
