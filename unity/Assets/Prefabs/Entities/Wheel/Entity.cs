﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : MonoBehaviour
{
    [SerializeField]
    private Player _player;

    private Vector3 _speedVector;

    // Start is called before the first frame update
    void Start()
    {
        _speedVector = new Vector3(_player.Speed + 0.5f ,0f,0f);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += _speedVector * Time.deltaTime;
    }

    public void OnPlayerCollisionEnter(Player.PlayerCollision2D playerCollision2D) {
        playerCollision2D.player.Die();
    }

    public void SetPlayer(Player player) {
        _player = player;
    }

    public void DestroyEntity() {
        Destroy(this.gameObject);
    }
}
