﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class Obstacle : MonoBehaviour
{
    [Header("Obstacle - Must set")]
    [SerializeField] private float minHeight;
    [SerializeField] private float maxHeight;
    [SerializeField] private bool useRandomPos = true;

    private void Start() {
        if(useRandomPos) {
            Vector3 position = transform.position;
            position.y = Random.Range(minHeight, maxHeight);
            transform.position = position;
        }
    }

    public abstract void AddListenerToEvent(UnityAction listener);
}
