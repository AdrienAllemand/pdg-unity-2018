﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Boost : Obstacle
{
    public UnityEvent _boostEvent = new UnityEvent();
    
    public void OnPlayerCollisionEnter(Player.PlayerCollision2D pc) {
        pc.player.Boost();
        _boostEvent.Invoke();
    }

    public override void AddListenerToEvent(UnityAction listener) {
        _boostEvent.AddListener(listener);
    }
}
