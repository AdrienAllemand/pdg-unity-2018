﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class DeadlyObstacle : Obstacle
{
    [SerializeField]
    GameObject explosion;
    [SerializeField]
    ShakeTransformEventData explosionShake;


    private UnityEvent _deadlyObstacleEvent = new UnityEvent();

    public override void AddListenerToEvent(UnityAction listener) {
        _deadlyObstacleEvent.AddListener(listener);
    }

    public void OnPlayerCollisionEnter(Player.PlayerCollision2D pc) {
        // deactivate the animator or he will reactivate stuff on his own
        GetComponent<Animator>().enabled = false;

        // dectivate collider
        GetComponent<Collider2D>().enabled = false;

        // deactivate sprites
        foreach (SpriteRenderer sr in GetComponentsInChildren<SpriteRenderer>()) {
            sr.enabled = false;
        }

        // activer l'explosion
        explosion.SetActive(true);
        Camera.main.GetComponent<CameraShake>().AddShakeEvent(explosionShake);

        // kill the playre
        pc.player.Die();

        _deadlyObstacleEvent.Invoke();
    }



}
