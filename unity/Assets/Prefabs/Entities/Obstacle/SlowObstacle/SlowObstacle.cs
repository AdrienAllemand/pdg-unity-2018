﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SlowObstacle : Obstacle
{

    private UnityEvent _slowObstacleEvent = new UnityEvent();

    public void OnPlayerCollisionEnter(Player.PlayerCollision2D pc) {
        pc.player.Slow();

        _slowObstacleEvent.Invoke();
    }

    public override void AddListenerToEvent(UnityAction listener) {
        _slowObstacleEvent.AddListener(listener);
    }


}
