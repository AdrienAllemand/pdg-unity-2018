﻿using UnityEngine;

public class Pattern : MonoBehaviour
{
    [Header("Pattern - must set")]
    [SerializeField] private float _length;


    public float GetLength() {
        return _length;
    }
}
