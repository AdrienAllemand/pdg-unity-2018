﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BinarySwipeDirection { VERTICAL, HORIZONTAL }

public class BinaryEnemy : Enemy {

    [Header("Binary Enemy - Must set")]
    [SerializeField] private GameObject swipeLine;

    private List<BinarySwipeDirection> _effectiveSwipes = new List<BinarySwipeDirection>();

    // Start is called before the first frame update
    void Start()
    {
        for(int i = 0; i < GetLife(); ++i) {
            int index = Random.Range(0, 2);
            _effectiveSwipes.Add((BinarySwipeDirection)index);
        }

        if(_effectiveSwipes[0] == BinarySwipeDirection.HORIZONTAL) {
            swipeLine.transform.Rotate(0,0,90,Space.Self);
        } else {
            //The bar is already vertical
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(GetPlayer() != null) {
            Vector3 newPos = new Vector3(transform.position.x, (GetPlayer().transform.position.y + 0.5f), 0);
            transform.position = newPos;
        }
        
    }


    public override void ProcessSwipe(SwipeInputs.Swipe swipe, RaycastHit2D hit) {

        //detecting one of the 8 directions (4 axis)
        Vector2 svector = new Vector2(swipe.xStart - swipe.xFinish, swipe.yStart - swipe.yFinish);
        float angle = Vector2.SignedAngle(Vector2.up, svector);

         if((_effectiveSwipes[0] == BinarySwipeDirection.VERTICAL && ((angle > -45 && angle <= 45) || (angle <= -135 || angle > 135)))
            ||(_effectiveSwipes[0] == BinarySwipeDirection.HORIZONTAL && ((angle > 45 && angle <= 135) || (angle <= -45 && angle > -135)))) {
            //if the swipe is right, the enemy lose one life point
            SetLife(GetLife() - 1);
            _effectiveSwipes.RemoveAt(0);
            GetLevelManager().AddPoints(GetValue());
         } else {
            Debug.Log("Wrong angle");
         }

         if(GetLife() <= 0) {
            GetEnemySpawner().RemoveAndRenewEnemy(this.gameObject);
            Die();
        }
    }

}
