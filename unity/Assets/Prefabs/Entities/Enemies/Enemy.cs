﻿using System.Collections;
using System.Collections.Generic;
using GooglePlayGames;
using UnityEngine;
using UnityEngine.UI;


public abstract class Enemy : MonoBehaviour
{

    [Header("Enemy - Must set")]
    [SerializeField] private int life;
    [SerializeField] private Text display;
    [SerializeField] private int _value;

    private EnemySpawner _enemySpawner;
    private GameObject _player;
    private Level _levelManager;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Level._testMode) {
            if (Input.GetKeyDown(KeyCode.Space)) {
                Die();
            }
        }
    }

    public abstract void ProcessSwipe(SwipeInputs.Swipe s, RaycastHit2D hit);

    public void Die() {
        if (PlayGamesPlatform.Instance.IsAuthenticated())
        {
            PlayGamesPlatform pgp = PlayGamesPlatform.Instance;
            pgp.UnlockAchievement(GPGSIds.achievement_first_blood, (bool success) => { Debug.Log("first blood badge won" + success); });
            pgp.IncrementAchievement(GPGSIds.achievement_robot_slayer, 1, (bool success) => { Debug.Log("robot_slayer badge add" + success); });
            pgp.IncrementAchievement(GPGSIds.achievement_serial_killer, 1, (bool success) => { Debug.Log("serial_killer badge add" + success); });
        }
        Destroy(this.gameObject);
    }

    protected int GetLife() {
        return life;
    }

    protected void SetLife(int life) {
        this.life = life;
    }

    protected EnemySpawner GetEnemySpawner() {
        return _enemySpawner;
    }

    public void SetEnemySpawner(EnemySpawner enemySpawner) {
        _enemySpawner = enemySpawner;
    }

    protected GameObject GetPlayer() {
        return _player;
    }

    public void SetPlayer(GameObject player) {
        _player = player;
    }

    public int GetValue() {
        return _value;
    }

    public void SetLevelManager(Level levelManager) {
        _levelManager = levelManager;
    }

    public Level GetLevelManager() {
        return _levelManager;
    }

}
