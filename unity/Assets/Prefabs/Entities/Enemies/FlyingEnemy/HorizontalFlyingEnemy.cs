﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HorizontalFlyingEnemy : Enemy
{
    [Header("HorizontalFlyingEnemy - Must set")]
    [SerializeField] private float _minOffsetX;
    [SerializeField] private float _maxOffsetX;
    [SerializeField] private float _minOffsetY;
    [SerializeField] private float _maxOffsetY;
    [SerializeField] private float _verticalMovment;
    [SerializeField] private float _timeBeforAttack;
    [SerializeField] private float _speed;

    private Animator anim;
    private float _time;
    private bool _movingUp;
    private Vector3 _currentPosition;
    private float _xStopPosition;
    private float _yStopPosition;
    private bool _chronoStarted;
    private float _maxYPos;
    private float _minYPos;


    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponentInChildren<Animator>();
        _xStopPosition = Random.Range(_minOffsetX, _maxOffsetX);
        _yStopPosition = GetYPosition();
        transform.position = new Vector3(transform.position.x, _yStopPosition, 0);
        _time = 0f;
        _chronoStarted = false;
        _movingUp = true;
        _maxYPos = GetPlayer().transform.position.y + _maxOffsetY <= transform.position.y + (_verticalMovment / 2) ?
            GetPlayer().transform.position.y + _maxOffsetY : transform.position.y + (_verticalMovment / 2);

        _minYPos = GetPlayer().transform.position.y + _minOffsetY >= transform.position.y - (_verticalMovment / 2) ?
            GetPlayer().transform.position.y + _minOffsetY : transform.position.y - (_verticalMovment / 2);
    }

    // Update is called once per frame
    void Update()
    {
        if(_time < _timeBeforAttack) {
            if(GetPlayer() != null && transform.position.x - GetPlayer().transform.position.x <= _xStopPosition) {
                anim.SetBool("idle", true);
                //VerticalMove();
                Vector3 newPos = new Vector3(GetPlayer().transform.position.x +_xStopPosition, _yStopPosition, 0);
                transform.position = newPos;

                if(_chronoStarted == false) {
                    _chronoStarted = true;
                } else {
                    _time += Time.deltaTime;
                }

            } else {
                //move until it reaches the position
            }
        } else {
            anim.SetBool("idle",false);
            DashTowardsPlayer();
        }

        if (Level._testMode) {
            if (Input.GetKeyDown(KeyCode.Space)) {
                Die();
            }
        }
    }

    public override void ProcessSwipe(SwipeInputs.Swipe swipe, RaycastHit2D hit) {
        //detecting one of the 8 directions (4 axis)
        Vector2 svector = new Vector2(swipe.xStart - swipe.xFinish, swipe.yStart - swipe.yFinish);
        float angle = Vector2.SignedAngle(Vector2.up, svector);

        if((angle > 45 && angle <= 135) || (angle <= -45 && angle > -135)) {
            //if the swipe is right, the enemy lose one life point
            SetLife(GetLife() - 1);
            GetLevelManager().AddPoints(GetValue());
        } else {
            Debug.Log("Wrong angle");
        }

        if(GetLife() <= 0) {
            //GetEnemySpawner().InstanciateFlyingEnemy();
            Die();
        }
    }

    public float GetYPosition() {
        float yPos = Random.Range(_minOffsetY, _maxOffsetY);
        return yPos;
    }

    private void DashTowardsPlayer() {
        if(GetPlayer() != null) {
            Vector3 direction = GetPlayer().transform.position - transform.position;
            Vector3 normalizedDirection = direction.normalized;
            transform.position += normalizedDirection * Time.deltaTime * _speed;
        }
    }

    public void OnPlayerCollisionEnter(Player.PlayerCollision2D pc) {
        pc.player.Slow();
        Die();
    }
}
