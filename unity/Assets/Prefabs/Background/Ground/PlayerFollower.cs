﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFollower : MonoBehaviour {

    [Header("PlayerFollower - Must set")]
    [SerializeField] private float _xOffset;

    private GameObject _player;
    private Vector3 _cameraPosition;

    // Update is called once per frame
    void Update()
    {
        if(_player != null) {
            FollowPlayer();
        } 
    }

    private void FollowPlayer() {

        //The camera will follow the player in the x direction
        _cameraPosition = new Vector3(_player.transform.position.x + _xOffset, transform.position.y, transform.position.z);
    
        transform.position = _cameraPosition;
    }

    public void SetPlayer(GameObject player) {
        _player = player;
    }

}
