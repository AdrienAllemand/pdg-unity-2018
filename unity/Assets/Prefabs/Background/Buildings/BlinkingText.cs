﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlinkingText : MonoBehaviour
{

    [SerializeField] Color colorOn;
    [SerializeField] Color colorOff;

    Text text;
    bool isOn = true;

    float timer = 0;

    // Start is called before the first frame update
    void Start()
    {
        text = GetComponent<Text>();


    }

    private void Update() {

        timer -= Time.deltaTime;

        if (timer < 0) {
            timer = Random.Range(0.1f, 0.5f);


            if (isOn) {
                text.color = colorOff;
                isOn = false;
            } else {
                text.color = colorOn;
                isOn = true;
            }
        }

    }

}
