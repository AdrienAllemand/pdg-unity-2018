﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingSpawner : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> _buildingPrefabs;
    [SerializeField]
    private float _destroyOffsetX;
    [SerializeField]
    private Vector3 _spawnOffsetVector;

    [SerializeField]
    private float _timeToSpawnMin;
    [SerializeField]
    private float _timeToSpawnMax;

    [SerializeField]
    private float _scaleMax;
    [SerializeField]
    private float _scaleMin;

    [SerializeField]
    [Range(0, 1)]
    private float speedDamp;

    private Player _player;
    private List<GameObject> _buildings = new List<GameObject>();
    private float timer;

    private void Start() {
        ResetTimer();
    }

    private void Update() {

        if(_player != null) {
            // update the timer
            timer -= Time.deltaTime;
        }

        // calculate points
        Vector3 spawnPoint = _spawnOffsetVector;
        spawnPoint.x += transform.position.x;

        float destroyPoint = _destroyOffsetX + transform.position.x;
        
        // cleaning and moving routine
        for(int i = _buildings.Count - 1; i != -1; i--) {

            if(_buildings[i].transform.position.x < destroyPoint) {
                Destroy(_buildings[i]);
                _buildings.RemoveAt(i);
            } else {
                if(_player != null) {
                    _buildings[i].transform.position += new Vector3(_player.Speed * speedDamp, 0, 0) * Time.deltaTime;
                }
            }
        }

        // Spawn des nouveaux batiments
        if(timer < 0) {
            ResetTimer();

            GameObject newBuilding = Instantiate(_buildingPrefabs[Random.Range(0, _buildingPrefabs.Count)], spawnPoint, Quaternion.identity);
            newBuilding.transform.localScale = Vector3.one * Random.Range(_scaleMin, _scaleMax);
            _buildings.Add(newBuilding);
        }
    }

    public void SetPlayer(Player player) {
        _player = player;
    }

    private void ResetTimer() {
        timer = Random.Range(_timeToSpawnMin, _timeToSpawnMax);
    }
   
}
