﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundSpawner : MonoBehaviour
{


    [Header("PlayerSmoothFollower - Must set in Inspector")]
    [SerializeField] private Transform _limitLeft;
    [SerializeField] private Transform _limitRight;

    [SerializeField] private List<GameObject> _buildings;

    [Header("PlayerSmoothFollower - Set via script")]
    [SerializeField] private Player _player;

    private float left;
    private float right;
    private float current;

    private float currentParser;

    public void Awake() {
        left = current = _limitLeft.transform.position.x;

    }

    public void Update() {
        
    }

    public void SetPlayer(Player player) {
        _player = player;
    }

}
