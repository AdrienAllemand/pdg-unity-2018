﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicSingleton : MonoBehaviour
{
    private static MusicSingleton instance;

    [SerializeField] private AudioClip musicMenu;
    [SerializeField] private AudioClip musicLevel;

    bool isMenu;
    AudioSource _as;

    public static MusicSingleton GetInstance() {
        return instance;
    }

    public void SetMusicMenu() {
        if (!isMenu) {
            isMenu = true;
            _as.clip = musicMenu;
            _as.Play();
            Debug.Log("Set music to menu");
        } else {

            Debug.Log("already music to menu");
        }
    }

    public void SetMusicLevel() {
        if (isMenu){
            isMenu = false;
            _as.clip = musicLevel;
            _as.Play();
            Debug.Log("Set music to level");
        } else {

            Debug.Log("already music to level");
        }
    }

    void Awake() {
        _as = GetComponent<AudioSource>();

        if (instance != null && instance != this) {
            Destroy(this.gameObject);
            return;
        } else {
            instance = this;
        }

        DontDestroyOnLoad(this.gameObject);
    }

}
