﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicChanger : MonoBehaviour
{

    public bool isMenu;

    // Start is called before the first frame update
    void Start()
    {
        MusicSingleton ms = MusicSingleton.GetInstance();
        if (isMenu) {
            ms.SetMusicMenu();
        } else {
            ms.SetMusicLevel();
        }
    }
}
