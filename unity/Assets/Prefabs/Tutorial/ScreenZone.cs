﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Zone {PLAYER, ENEMY };

public class ScreenZone : MonoBehaviour
{
    [Header("Zone - Must set")]
    [SerializeField] private Zone _zone;

    private GameObject _player;

    private float _ySize;
    private float _yPos;

    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, Camera.main.WorldToScreenPoint(_player.transform.position).x);
    }

    // Update is called once per frame
    void Update()
    {
        UpdateSize();
    }

    public void SetPlayer(GameObject player) {
        _player = player;
    }

    private void UpdateSize() {
        if(_player != null) {
            gameObject.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, Camera.main.WorldToScreenPoint(_player.transform.position).x);
        }
    }


}
