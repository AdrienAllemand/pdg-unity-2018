﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class tuto_init : StateMachineBehaviour
{

    [SerializeField] private DisplayTutorial _canvas;

    [SerializeField] private GameObject _playerPrefab;
    [SerializeField] private Transform _playerSpawner;
    [SerializeField] private Transform _playerStopSpot;
    [SerializeField] private GameObject _cameraHolderPrefab;
    [SerializeField] private GameObject _groundPrefab;
    [SerializeField] private GameObject _swipeDetectorPrefab;


    [Header("Obstacle and Enemies Prefabs")]
    [SerializeField] private GameObject _slowObstaclePrefab;
    [SerializeField] private GameObject _deadlyObstaclePrefab;
    [SerializeField] private GameObject _wheelPrefab;
    [SerializeField] private GameObject _boostPrefab;
    [SerializeField] private GameObject _enemyPrefab;

    private GameObject _swipeDetector;
    private string _sceneName = "menuBen";
    private bool _cameraLinkedToPlayer;
    private GameObject _player;
    private GameObject _cameraHolder;
    private GameObject _ground;
    private GameObject _entity;
    private Transform _deathPosition;

    private bool _hasJump;
    private bool _hasDuck;
    private bool _hasJumpOverObstacles;
    private bool _hasDuckBelowObstacles;
    private bool _hasCatchedBoost;
    private bool _hasAvoidMine;
    private bool _hasSwipedEnemy;
    private bool _hasTouchSlowObstacle;
    private bool _hasTouchDeadlyObstacle;
    private bool _hasMissedBoost;
    private bool _hasTouchEnemy;

    private List<Image> _tutorialFirstDialogs = new List<Image>();
    private List<Image> _tutorialAvoidObstacles = new List<Image>();
    private List<Image> _tutorialExplainMovments = new List<Image>();
    private List<Image> _tutorialSecondDialogs = new List<Image>();
    private List<Image> _tutorialThirdDialogs = new List<Image>();

    private List<Image> _tutorialFailures = new List<Image>();
    private List<Image> _tutorialGoalReached = new List<Image>();

    private List<GameObject> _tutorialZones = new List<GameObject>();
    private List<Image> _tutorialArrows = new List<Image>();

    private void Awake() {
        _player = Instantiate(_playerPrefab, _playerSpawner.position, _playerSpawner.rotation);
        _cameraHolder = Instantiate(_cameraHolderPrefab);
        _ground = Instantiate(_groundPrefab);
        _swipeDetector = Instantiate(_swipeDetectorPrefab);
    }

    void Start() {
        _player.GetComponent<Player>().SetJumpAuthorization(false);
        _player.GetComponent<Player>().SetDuckAuthorization(false);
        _cameraLinkedToPlayer = false;

        _swipeDetector.GetComponent<SwipeInputs>().SetPlayer(_player);

        //For the tutorial the buildings in the background are inactive
        Transform buildings = Camera.main.transform.Find("Buildings Holder");
        if(buildings == null) {
            Debug.Log("buildings is null");
        }
        buildings.gameObject.SetActive(false);


        _tutorialFirstDialogs = _canvas.GetTutorialFirstDialogs();
        _tutorialAvoidObstacles = _canvas.GetTutorialAvoidObstacles();
        _tutorialExplainMovments = _canvas.GetTutorialExplainMovment();
        _tutorialSecondDialogs = _canvas.GetTutorialSecondDialogs();
        _tutorialThirdDialogs = _canvas.GetTutorialThirdDialogs();

        _tutorialFailures = _canvas.GetTutorialFailures();
        _tutorialGoalReached = _canvas.GetTutorialReachGoal();

        _tutorialZones = _canvas.GetTutorialZones();
        foreach(GameObject o in _tutorialZones) {
            o.GetComponent<ScreenZone>().SetPlayer(_player);
        }

        _tutorialArrows = _canvas.GetTutorialArrows();

        _hasJump = false;
        _hasDuck = false;
        _hasJumpOverObstacles = false;
        _hasDuckBelowObstacles = false;
        _hasCatchedBoost = false;
        _hasAvoidMine = false;
        _hasSwipedEnemy = false;
        _hasTouchSlowObstacle = false;
        _hasTouchDeadlyObstacle = false;
        _hasMissedBoost = false;
        _hasTouchEnemy = false;
    }

    public void HasJump() {
        _hasJump = true;
    }

    public void HasDuck() {
        _hasDuck = true;
    }


    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
