﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayTutorial : MonoBehaviour
{

    [SerializeField] private List<Image> _tutorialFirstDialogs = new List<Image>();
    [SerializeField] private List<Image> _tutorialAvoidObstacles = new List<Image>();
    [SerializeField] private List<Image> _tutorialExplainMovments = new List<Image>();
    [SerializeField] private List<Image> _tutorialSecondDialogs = new List<Image>();
    [SerializeField] private List<Image> _tutorialThirdDialogs = new List<Image>();

    [SerializeField] private List<Image> _tutorialFailures = new List<Image>();
    [SerializeField] private List<Image> _tutorialGoalReached = new List<Image>();

    [SerializeField] private List<GameObject> _tutorialZones = new List<GameObject>();
    [SerializeField] private List<Image> _tutorialArrows = new List<Image>();


    public List<Image> GetTutorialFirstDialogs() {
        return _tutorialFirstDialogs;
    }

    public List<Image> GetTutorialAvoidObstacles() {
        return _tutorialAvoidObstacles;
    }

    public List<Image> GetTutorialExplainMovment() {
        return _tutorialExplainMovments;
    }

    public List<Image> GetTutorialSecondDialogs() {
        return _tutorialSecondDialogs;
    }

    public List<Image> GetTutorialFailures() {
        return _tutorialFailures;
    }

    public List<Image> GetTutorialReachGoal() {
        return _tutorialGoalReached;
    }

    public List<GameObject> GetTutorialZones() {
        return _tutorialZones;
    }

    public List<Image> GetTutorialArrows() {
        return _tutorialArrows;
    }

    public List<Image> GetTutorialThirdDialogs() {
        return _tutorialThirdDialogs;
    }

}
