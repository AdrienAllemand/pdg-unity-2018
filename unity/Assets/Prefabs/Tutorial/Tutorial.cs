﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class Tutorial : MonoBehaviour { 

    [Header("Tutorial - Must set")]

    [SerializeField] private DisplayTutorial _canvas;

    [SerializeField] private GameObject _playerPrefab;
    [SerializeField] private Transform _playerSpawner;
    [SerializeField] private Transform _playerStopSpot;
    [SerializeField] private GameObject _cameraHolderPrefab;
    [SerializeField] private GameObject _groundPrefab;
    [SerializeField] private GameObject _swipeDetectorPrefab; 
    

    [Header("Obstacle and Enemies Prefabs")]
    [SerializeField] private GameObject _slowObstaclePrefab;
    [SerializeField] private GameObject _deadlyObstaclePrefab;
    [SerializeField] private GameObject _wheelPrefab;
    [SerializeField] private GameObject _boostPrefab;
    [SerializeField] private GameObject _enemyPrefab;

    private IEnumerator beginLoopCoroutine;
    private IEnumerator coroutineFail;
    private IEnumerator coroutineWin;


    private GameObject _swipeDetector;
    private string _sceneName = "menuBen";
    private bool _cameraLinkedToPlayer;
    private GameObject _player;
    private GameObject _cameraHolder;
    private GameObject _ground;
    private GameObject _entity;
    private IEnumerator _coroutine;
    private Transform _deathPosition;

    private bool _hasJump;
    private bool _hasDuck;
    private bool _hasJumpOverObstacles;
    private bool _hasDuckBelowObstacles;
    private bool _hasCatchedBoost;
    private bool _hasAvoidMine;
    private bool _hasSwipedEnemy;
    private bool _hasTouchSlowObstacle;
    private bool _hasTouchDeadlyObstacle;
    private bool _hasMissedBoost;
    private bool _hasTouchEnemy;

    private List<Image> _tutorialFirstDialogs = new List<Image>();
    private List<Image> _tutorialAvoidObstacles = new List<Image>();
    private List<Image> _tutorialExplainMovments = new List<Image>();
    private List<Image> _tutorialSecondDialogs = new List<Image>();
    private List<Image> _tutorialThirdDialogs = new List<Image>();

    private List<Image> _tutorialFailures = new List<Image>();
    private List<Image> _tutorialGoalReached = new List<Image>();

    private List<GameObject> _tutorialZones = new List<GameObject>();
    private List<Image> _tutorialArrows = new List<Image>();

    public delegate void NoArgumentFunction();

    private void Awake() {
        _player = Instantiate(_playerPrefab, _playerSpawner.position, _playerSpawner.rotation);
        _cameraHolder = Instantiate(_cameraHolderPrefab);
        _ground = Instantiate(_groundPrefab);
        _swipeDetector = Instantiate(_swipeDetectorPrefab);
    }

    // Start is called before the first frame update
    void Start()
    {
        _player.GetComponent<Player>().SetJumpAuthorization(false);
        _player.GetComponent<Player>().SetDuckAuthorization(false);
        _cameraLinkedToPlayer = false;

        _swipeDetector.GetComponent<SwipeInputs>().SetPlayer(_player);
        
        //For the tutorial the buildings in the background are inactive
        Transform buildings = Camera.main.transform.Find("Buildings Holder");
        if(buildings == null) {
            Debug.Log("buildings is null");
        }
        buildings.gameObject.SetActive(false);


        _player.GetComponent<Player>().AddJumpEventListener(HasJump);
        _player.GetComponent<Player>().AddDuckEventListener(HasDuck);

        _coroutine = StartTutorial();

        _tutorialFirstDialogs = _canvas.GetTutorialFirstDialogs();
        _tutorialAvoidObstacles = _canvas.GetTutorialAvoidObstacles();
        _tutorialExplainMovments = _canvas.GetTutorialExplainMovment();
        _tutorialSecondDialogs = _canvas.GetTutorialSecondDialogs();
        _tutorialThirdDialogs = _canvas.GetTutorialThirdDialogs();

        _tutorialFailures = _canvas.GetTutorialFailures();
        _tutorialGoalReached = _canvas.GetTutorialReachGoal();

        _tutorialZones = _canvas.GetTutorialZones();
        foreach(GameObject o in _tutorialZones) {
            o.GetComponent<ScreenZone>().SetPlayer(_player);
        }

        _tutorialArrows = _canvas.GetTutorialArrows();

        _hasJump = false;
        _hasDuck = false;
        _hasJumpOverObstacles = false;
        _hasDuckBelowObstacles = false;
        _hasCatchedBoost = false;
        _hasAvoidMine = false;
        _hasSwipedEnemy = false;
        _hasTouchSlowObstacle = false;
        _hasTouchDeadlyObstacle = false;
        _hasMissedBoost = false;
        _hasTouchEnemy = false;
}

    // Update is called once per frame
    void Update()
    {
        if(_cameraLinkedToPlayer == false && _player.transform.position.x >= _playerStopSpot.position.x) {
            StartCoroutine(_coroutine);
            _cameraLinkedToPlayer = true;
        }
    }


    private IEnumerator StartTutorial() {

        //The camera is attached to the player
        _cameraHolder.GetComponent<PlayerSmoothFollower>().SetPlayer(_player);
        _ground.GetComponent<PlayerFollower>().SetPlayer(_player);
        

        //Player stops moving;
        _player.GetComponent<Player>().SetStop(true);

        //The first dialogs pass
        int startDialogSize = _tutorialFirstDialogs.Count;

        /*
        for(int i = 0; i < startDialogSize; ++i) {
            _tutorialFirstDialogs[i].gameObject.SetActive(true);
            yield return new WaitForSeconds(3f);
            _tutorialFirstDialogs[i].gameObject.SetActive(false);
        }
        */


        //Player moves again
        _player.GetComponent<Player>().SetStop(false);
        _tutorialZones[0].gameObject.SetActive(true);

        //The player can now jump  (an arrow appears to indicate how  to swipe to jump && a square indicates where the player can swipe)
        _tutorialExplainMovments[0].gameObject.SetActive(true);
        _tutorialArrows[0].gameObject.SetActive(true);
        _player.GetComponent<Player>().SetJumpAuthorization(true);

        yield return new WaitUntil(() => _hasJump == true);

        _tutorialExplainMovments[0].gameObject.SetActive(false);
        _tutorialArrows[0].gameObject.SetActive(false);
        Debug.Log("COUCOU");

        //Wait 1 sec before next dialog (We wait that the jump is finished) and instantiatie serie of enemies
        yield return new WaitForSeconds(1f);
        _tutorialZones[0].gameObject.SetActive(false);


        yield return GenerateObstacles(
            _tutorialAvoidObstacles[0], 
            _tutorialFailures[0], 
            _tutorialGoalReached[0], 
            _slowObstaclePrefab, 
            0.5f, 
            _hasJumpOverObstacles, 
            _hasTouchSlowObstacle, 
            15f, 
            1, 
            HasTouchSlowObstacle);

        

        //new Dialogs to indicate the player can now duck
        _tutorialExplainMovments[1].gameObject.SetActive(true);
        yield return new WaitForSeconds(2f);
        _tutorialExplainMovments[1].gameObject.SetActive(false);

        //The player can now duck (an arrow appears to indicate how  to swipe to jump && a square indicates where the player can swipe)
        _tutorialZones[0].gameObject.SetActive(true);
        _tutorialExplainMovments[2].gameObject.SetActive(true);
        _tutorialArrows[1].gameObject.SetActive(true);
        _player.GetComponent<Player>().SetDuckAuthorization(true);  

        yield return new WaitUntil(() => _hasDuck == true);

        _tutorialExplainMovments[2].gameObject.SetActive(false);
        _tutorialArrows[1].gameObject.SetActive(false);
        _tutorialZones[0].gameObject.SetActive(false);

        //Wait 1 sec before next dialog (We wait that the jump is finished) and instantiatie serie of enemies
        yield return new WaitForSeconds(1f);

        //When the player has swiped instantiation of three slow obstacle the player must duck under
        //Wait 0.5 sec before next dialog (We wait that the jump is finished)
        yield return GenerateObstacles(
            _tutorialAvoidObstacles[1], 
            _tutorialFailures[0], 
            _tutorialGoalReached[1], 
            _slowObstaclePrefab, 
            1.5f, 
            _hasDuckBelowObstacles,
            _hasTouchSlowObstacle , 
            15f,
            1, 
            HasTouchSlowObstacle);


        //Wait before next step
        yield return new WaitForSeconds(0.5f);

        //Dialog indicates that there is some boost on the way 
        _tutorialExplainMovments[4].gameObject.SetActive(true);
        yield return new WaitForSeconds(2f);
        _tutorialExplainMovments[4].gameObject.SetActive(false);

        //Instantiate a boost => try to catch it! 
        yield return GenerateObstacles(
            _tutorialAvoidObstacles[2],
            _tutorialFailures[0],
            _tutorialGoalReached[2],
            _boostPrefab,
            2f,
            _hasCatchedBoost,
            _hasMissedBoost,
            15f,
            1,
            HasMissedBoost);


        //Wait before next step
        yield return new WaitForSeconds(0.5f);

        // Dialog that indiacates to be really careful with deadly obstacles
        _tutorialExplainMovments[5].gameObject.SetActive(true);
        yield return new WaitForSeconds(2f);
        _tutorialExplainMovments[5].gameObject.SetActive(false);

        //instantiate a deadly obstacle 
        yield return GenerateObstacles(
            _tutorialAvoidObstacles[3],
            _tutorialFailures[1],
            _tutorialGoalReached[3],
            _deadlyObstaclePrefab,
            2f,
            _hasAvoidMine,
            _hasTouchDeadlyObstacle,
            15f,
            1,
            HasTouchDeadlyObstace);


        //Wait before next step
        yield return new WaitForSeconds(0.5f);

        //Dialog that indicates what's on the right
        foreach(Image i in _tutorialSecondDialogs) {
            i.gameObject.SetActive(true);
            yield return new WaitForSeconds(3);
            i.gameObject.SetActive(false);
        }

        //instantiate a flying enemy 
        yield return GenerateObstacles(
            _tutorialExplainMovments[6],
            _tutorialFailures[2],
            _tutorialGoalReached[4],
            _enemyPrefab,
            7f,
            _hasSwipedEnemy,
            _hasTouchEnemy,
            15f,
            1,
            HasTouchEnemy);


        ////Wait before next step
        yield return new WaitForSeconds(0.5f);

        //The player stops
        _player.GetComponent<Player>().SetStop(true);

        //Dialog indicating the wheel
        _tutorialThirdDialogs[0].gameObject.SetActive(true);
        yield return new WaitForSeconds(3f);
        _tutorialThirdDialogs[0].gameObject.SetActive(false);

        //The player turns and the camera move backwards to see the wheel 
        _tutorialThirdDialogs[1].gameObject.SetActive(true);
        Vector3 wheelPos = new Vector3(_player.transform.position.x - 15, 0, 0);
        _entity = Instantiate(_wheelPrefab, wheelPos, Quaternion.identity);
        _cameraHolder.GetComponent<PlayerSmoothFollower>().SetPlayer(_entity);
        yield return new WaitForSeconds(3f);
        _cameraHolder.GetComponent<PlayerSmoothFollower>().SetPlayer(_player);
        _tutorialThirdDialogs[1].gameObject.SetActive(false);

        _tutorialThirdDialogs[2].gameObject.SetActive(true);
        yield return new WaitForSeconds(2f);
        _tutorialThirdDialogs[2].gameObject.SetActive(false);

        //The player run again, the camera doesn't follow, the player quit the camera field. The wheel pass. 
        _tutorialThirdDialogs[3].gameObject.SetActive(true);
        _player.GetComponent<Player>().SetStop(false);
        yield return new WaitForSeconds(2f);
        _tutorialThirdDialogs[3].gameObject.SetActive(false);
        _entity.GetComponent<Entity>().SetPlayer(_player.GetComponent<Player>());
        _cameraHolder.GetComponent<PlayerSmoothFollower>().SetPlayer(null);

        yield return new WaitUntil(() => _entity.transform.position.x >= 15);

        SceneManager.LoadScene(_sceneName, LoadSceneMode.Single);


















        // Dialog to indicate that there is enemies to swipe horizontally  a rectangle indocates the enemy zone 

        //instantiate an enemy (an arrow indicates how it can be swiped)

    }


    public void HasJump() {
        _hasJump = true;
    }

    public void HasDuck() {
        _hasDuck = true;
    }

    public void HasJumpOverObstacles() {
        _hasJumpOverObstacles = true;
    }

    public void HasDuckBelowObstacles() {
        _hasDuckBelowObstacles = true;
    }

    public void HasCatchedBoost() {
        _hasCatchedBoost = true;
    } 

    public void HasAvoidMine() {
        _hasAvoidMine = true;
    }
    
    public void HasSwipedEnemy() {
        _hasSwipedEnemy = true;
    }

    public void HasTouchSlowObstacle() {
        _hasTouchSlowObstacle = true;
    }

    public void HasTouchDeadlyObstace() {
        _hasTouchDeadlyObstacle = true;
        while(_player != null) {
            //wait
        }
        _player = Instantiate(_playerPrefab, _deathPosition.transform.position, Quaternion.identity);
        _cameraHolder.GetComponent<PlayerSmoothFollower>().SetPlayer(_player);
    }

    public void HasMissedBoost() {
        _hasMissedBoost = false;
    }

    public void HasTouchEnemy() {
        _hasTouchEnemy = true;
    }


    public IEnumerator DisplayDialog(GameObject g, float sec) {
        g.SetActive(true);
        yield return new WaitForSeconds(sec);
        g.SetActive(false);
    }



    private IEnumerator GenerateObstacles(
        Image textObstacle, 
        Image failure, 
        Image achievement, 
        GameObject obstaclePrefab, 
        float yPos, 
        bool endCondition, 
        bool failingEndCondition,
        float distanceBetweenObstacles, 
        int numberOfObstacles,
        UnityAction collideWithObstacle) 
        {
        
        textObstacle.gameObject.SetActive(true);
        yield return new WaitForSeconds(2f);
        textObstacle.gameObject.SetActive(false);

        bool hasInstantiateObstacles = false;
        float posX = 0;

        while(endCondition == false) {
        
           Debug.Log("In the while loop");
        
          
        
               Debug.Log("Instantiation of the obstacles");
        
               posX = _player.transform.position.x + distanceBetweenObstacles;
               //instantiation of obstacles
               for(int i = 0; i < numberOfObstacles; ++i) {
                   Vector3 pos = new Vector3(posX, yPos, 0f);
                   GameObject obstacle = Instantiate(obstaclePrefab, pos, Quaternion.identity);
                   obstacle.GetComponent<Obstacle>().AddListenerToEvent(collideWithObstacle);
                   posX += distanceBetweenObstacles;
               }


            yield return new WaitUntil(() => posX <= _player.transform.position.x);


               Debug.Log("Reached the end pos");
               if(failingEndCondition == false) {
                   //When the serie is completed => well done!
                   achievement.gameObject.SetActive(true);
                   yield return new WaitForSeconds(1);
                   achievement.gameObject.SetActive(false);
                   
                   endCondition = true;
               } else {
                   //while the serie is not completed, => try again  (the serie is reinstantiated)
                   
                   failure.gameObject.SetActive(true);
                   yield return new WaitForSeconds(1);
                   failure.gameObject.SetActive(false);
               
        
                   
               }
           


        }
    }



}
