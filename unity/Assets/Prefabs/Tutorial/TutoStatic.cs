﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TutoStatic : MonoBehaviour
{
    public void BackToMenu() {
        SceneManager.LoadScene("MenuBen", LoadSceneMode.Single);
    }
}
