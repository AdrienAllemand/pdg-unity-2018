﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestColorLerp : MonoBehaviour
{
    [SerializeField]
    private float speed;

    [SerializeField]
    private Material mat;
    

    // Update is called once per frame
    void Update()
    {

        mat.color = Color.HSVToRGB(Mathf.PingPong(Time.time * speed, 1), 1, 1);
    }
}
