Allemand Adrien, Fluckiger Nathan, Krug Loyse,  
 Pagani Mika, Thomas Benjamin

# <center> Objectives for the first demo </center>


## <center> Our project </center>

<p style="text-align:justify;"> The goal of our project is to developpe the most completely possible an Android game. We have defined three major axes to explore and work on to reach this goal. </p>


#### Project's organization:

<p style="text-align:justify;">
We must define multiple guidelines to ensure a good cohesion between all the team's developers.
 One will describe the rules to follow during code's redaction like variable's name, comments, tests and more specifics notions linked to the usage of Unity and there will also be another one for the usage of git.
We will set up gitlab to have a continues testing integration of the Unity's code and finally, we must begin the redaction of the main report.
</p>

#### Game mechanics:

<p style="text-align:justify;">
By the time we reach the date of the first demo, we want to have a beta version of the game that will have the following mechanics implemented.
The first thing that will appear at the app's opening will be a menu with three sections. There will be a score section, a configuration section and a game section.
For that, we must define in a first time the graphic chart of the game. The ambiance that will come out of the background, the menu but also the game itself will have to be set. There will also be an ambiance theme and sound effects when an action occurs.
</p>

###### - Configuration section:

<p style="text-align:justify;">
The configuration section won't be completely usable but will contain features to change the language of the game and adjust the graphics.
</p>

###### - Game section:

<p style="text-align:justify;">
If the user press on the game section, he will see all the different accessible levels. There will be only one level for the first demo.
The player will play a character who automatically run. He will be chased by a deadly thing, probably a
</p>

## <center> NEON SNAIL </center>

<p style="text-align:justify;">
or something else, whatever. There will be obstacles to avoid and some to destroy. If the hero encounters an obstacle that he couldn't destroy in time, he will be slow down by the impact. The time lost by the impact will be crucial, because of the </p>

## <center> GLORIOUS NEON SNAIL </center>

<p style="text-align:justify;">

The screen will be divide in two parts, the left one and the right one, that will contain specific commands to interact with the environment.
The commands on the left part will allow the player to interact with his character. It will jump if he swipes up, bend down if he swipes down, accelerate if he swipes right and slow down if he swipes left.
The commands on the right part will allow the player to attack and destroy the obstacles if they can be.
The game end when the player hit a deadly obstacle or when the </p>

## <center> GLORIOUS ALMIGHTY NEON SNAIL </center>

<p style="text-align:justify;"> catch up the player. At the player's death, the game will save the score and return to the level menu.

A randomized background would be nice to enhance the movement of the player. The background should be able to spawn stuff as the player moves. Multiple "plans" must be set including far-away, mid and close by. Each plan must have a different speed value giving a perspective impression. A Foreground sometimes hiding the view to the player would be nice to have.

</p>

###### - Score section:

<p style="text-align:justify;">
There will be a scoring system in the game calcultated on the distance, the number of destroyed obstacles and maybe time. All the results will be published on GooglePlay

</p>

#### Promotion:

<p style="text-align:justify;">

Another major axe of our project result in the promotion of our game, but also the continues feedback of the users. We want them to fully participate to the development of the game by testing and giving us their review of the different version to give the best possible experience when the game will be finished.
For that, we will create an official association to promote our game, a twitter acount and a website to make it easier for the users to comment and share our project. We will also create a Discord Channel with diverses bots to help us with the information's flow, but mainly to talk directly with the testers of our game, organisze events and getting their feedbacks.


</p>

## <center> FEAR THE GLORIOUS ALMIGHTY NEON DEMIGOD SNAIL </center>
