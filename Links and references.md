# Links and references

## Game Dev

La chaîne Game Developper Conferences est trop bien : 
**GDC** : https://www.youtube.com/channel/UC0JB7TSe49lg56u6qH8y_MQ

Des idées de platformes pour déployer le jeu et avoir du feedback
Get People to play your game : https://www.youtube.com/watch?v=ZeqxwdA-2UU

## Unity

Deux chaînes auquelles il faut absolument s'abonner si on veut apprendre unity :
**Brackeys** : https://www.youtube.com/user/Brackeys
**BlackThornProd** : https://www.youtube.com/channel/UC9Z1XWw1kmnvOOFsj6Bzy2g

UnityHub permet de gérer facilement la version de Unity qui est utilisée pour chaque projet :
**UnityHub** : https://docs.unity3d.com/Manual/GettingStartedInstallingHub.html

Nous allons utiliser Unity 2018.3.0b1 qui n'est **pas** la dernière version :
**Unity2018.3.0b1** : https://unity3d.com/fr/unity/beta/unity2018.3.0b1
A installer depuis ce lien puis "lier" a unity hub depuis ce dernier.

**Tests Unitaires** intégré dans unity :
https://docs.unity3d.com/Manual/testing-editortestsrunner.html

## Gitlab

Utiliser Gitlab dans un développement **agile**
https://about.gitlab.com/2018/03/05/gitlab-for-agile-software-development/

Une application Android gratuite pour gérer Gitlab depuis son natel :
**LabCoat App**: https://play.google.com/store/apps/details?id=com.commit451.gitlab

Un bot Gitlab pour discord :
Yappy : https://bots.discord.pw/bots/303661490114789391

Continuous Integration, Tests Unitaires pour unity automatisés avec GitLab
CI : https://gitlab.com/gableroux/unity3d-gitlab-ci-example

## Discord

Un bon tuto pour mettre en place un bot discord **/!\ avoir adblock /!\ **
https://www.digitaltrends.com/gaming/how-to-make-a-discord-bot/

Un meilleur tuto pour bot discord
https://www.youtube.com/watch?v=024upsEuHaU