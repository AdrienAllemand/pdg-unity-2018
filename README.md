# PDG Unity 2018

By : Adrien Allemand, Benjamin Thomas, Loyse Krug, Nathan Flückiger, Mika Pagani




## Introduction

As part of a 3rd year's course at the HEIG-VD, we were asked to carry out a group project, from the beginning to the end. 
We chose to develope a Android casual game in order to release it with all the surroundings, supposing we were a new start-up thying to dig its place into the Video Game Market.
The game we chose to develop is a 2D, 3rd person runner, in a New Retro Wave ambiance, called NEON Swipejutsu.



## Quick links

Website: http://neonswipejutsu.flyingdutchman.codes/
Discord: https://discord.gg/dFxRtB
Twitter: https://twitter.com/swipejutsu



## Objectives

For this project we chose to develop a game with all its surroundings. We want to follow the entire process of releasing a game with all the development, marketing and lauching parts.
The project includes the following issues:

- Andoid Game development
- Release on the Google Play Store
- Integration of Achivment system
- Integration of Ads and payment system to remove them
- Creation of a Discord Channel to communicate with the community
- Creation of a web site to document the game, its development process, and the team dynamics
- Creation of a Twitter account
- Officialize our team through the Creation of an Association

## Team

Our team is composed of 5 members :

Adrien Allemand
Benjamin Thomas
Loyse Krug
Nathan Flückiger
Mika Pagani

We are all students in Software Engineering at the HEIG-VD in 3rd year.

### Roles

As a team of Software Engineering students, out objectif is not only to develop an App, but also to releasing it, involving marketing and community networking.
Therefore, during this project each member of the team will have the role of a developer, but also marketing specialist, communication expert and other knighted soldier.

### Scrum Roles

Adrien Allemand - Project Owner
Mika Pagani - Scrum Master

### Taram Association

In order to release  the game on the Google Play Store and add Ads and payment system, and be we needed to create an entity that groups all members of the team. Therefore we decided to officialize the team through an association for which we would develop the game.
We decided to create the Taram association.

## Team Guidelines

As we never worked together on such a project, we decided to establish guidlines (for code, git and organisation) in order to unify the project form the start.
Find the guidlines at chapter : Quick links

## Tools and technologies

For the project, we use sevral technologies and API

### Unity with C#

For the development of the game itself we chose to use the Unity Engine with C# language.

### Google Play API

For we want to release the game on the Google Play Store, we will use the Google Play API as integration tool.

### GitLab

We use GitLab as a versioning tool for it offers many others services as issues managment and Scrum boards. (we discovered during the project that gitlab isn't  a good tool to use with unity (which integrates its own integration platform))

### JavaScrip, Node JS & Discord API

We use javascrip with the Discord API to create Discord bots. Thoses bots are used to create dynamisme in the community Discord channel.

### WordPress

We want to use WordPress to create a website for the game.

### Firebase

A framework used to add statistics to our game. 



## Communication Platforms

### Website

We want to have a website for the game, in order to document it but not only.
We wish to use this website to document our progress as a team during the devlopment. We would like to do so in order to keep track of this experience and hopefully  help and encourage futur teams that would like to embark on the same adventure. Therefore we would like to regulary post news about our progress and our expreience inside the team.

Website: http://neonswipejutsu.flyingdutchman.codes/

### Discord

We installed a Discord server for the game in order to welcom a potential community of players. The platform is supposed to be dynamic and animated by ourself and hopefully by sevral bots during all the game devlopment.

Link: https://discord.gg/dFxRtB

### Twitter

Same as Discord, we chose to open a twitter account in the name of the team to present the game to potential futur players, posting regular tweets all along the devlopment

Link: https://twitter.com/swipejutsu



## Game Concept

NEON Swipejutsu is an Andoid side scroller game that focus on finger swipes.
The user plays as a ninja running  forward, avoiding obstacles and swiping enemies. The goal is to go as far as possible with a increasing difficulty.
We would like the game to have diferent level that are unlocked when the player has reached a certain distance. Passing a level would unlock the infinite level starting with the level difficulty.
Thus there yould be different play mode:

the adventure mode that make the player run through all the levels
the infinite mode where the player can play with an increasing difficulty and try to reach the high score
the tutorial mode ?

The game will have access to to google play account of the player to 



## Event

At the beginning of the project, we took the challenge to add another deadline to our game developement: 

The PolyLAN 32 Event 

The event took place at the SwissTech Center at the EPFL, in November 2-4 2018)

The challenge was to have a functionnal demo of the game during the event, in order to apply our user testing step in the developement cycle.

The challenge succeeded and we've had the opportunity to present our game to the PolyLAN32. 



### Part of the contest

As we asked the PolyLAN staff if we could show our game in their event, not only the accepted, but they offered us the opportunity to make it part of the video game contest of the Event. 

The Activity took place on Sunday November 4 and had a great success, 16 players took their chance and won points for the contest. 



### User feedback

For the event we prepared a user feedback form to gather players opinons on the game. We got 11 answers the to our survey. And decided to base our Demo 2 objectives on it.  



## Unity 

Our project uses the Unity 3D Engine to work, we used the personnal version which is free. 



### Unity interface

Here is a screen of the Unity interface:

![1548289108494](./images/unityDesktop.png)

It is devided in many windows: 

The Project window, in which we can find all the hierachy of files concerning our application. 

The Scene Window in which we can give the right positions to our gameObjects 

The Game Window in which the game can be run to preview the way the game work (When a version of the game is ready, we can build an apk or an exe, depending on the developement platform)



### Unity Elements

In Unity we work with GameObjects, Scenes, Prefabs, Scipts. 

The **Scene** objects contains all the elements to create a scene of the game. It is composed of GameObjects. 



A  **GameObject** is an element which can be anything. A cube, a spite, a sound,  a controller, a model. It can itself be composed of others GameObject. It is an instance of an object. There is a lot of game objects alrealdy configurated, which are used as base of more complexe GameObject. 

 

It is possible to attach a **Script** to a  GameObject in order to give it a behaviour. In Unity, most of the scripts used in game objects extend a class MonoBehaviour, which gives the base to implements game mecanics, with the four methods Awake(), Start(), Update() and FixedUpdate(). 

Awake and Start are used to give the start values to an object

Update and FixedUpdate are used to give a behaviour to the object that will be executed every frame.



Finally, **Prefabs** are configurated  instances of meta game objects. They can be used in different contexts to instantiatie a GameObject with those propreties. 



### Our unity project

#### Unity version 

We chose to work with a specific version of Unity (2018.3.0b1) because it offers a new service to the developers with the oppotunity to create movment animations from sprites. The problem with this version of unity is that it is still a beta version, therefore we were blocked at some points of our project (mainly with the GitLab Continuous Integration)



#### Config for Android

In order to create an .apk and not a .exe file, we had to configure the Engine in buildings settings. Every build of the application creates a file which can have a verison number. 



#### Scenes

In our project, we have 3 scenes: 

- A Preload scene , in which run scripts to instantiate GameObjects which will be kept in the navigation between the scenes
- A Menu Scene which is the first ont we see when we run the game
- A Level scene, which contains the level. 



#### Files hierarchy

In a unity project the root of all the elements added to the game is the `Assets/` folder. 

Here is the structure of our Assets folder:

![](./images/FilesHierarchy.PNG)

The different modules (Assets) added to our project were added directly in the Assets folder (example: Firebase, GooglePlayGames, PlayServicesResolver, Plugins)

In this file we also find different folders as Scene, Scripts, Sounds, Sprites and Prefabs. 

in the Prefab file we can find most of the elements of the game for we wanted to save their configuration. 

The files Scipts, Sounds, Sprites contains elements, but they are mosty doubles for we tried, in our hierarcy, to keep together all the elements that create a Prefab. Example: The player Prefab is kept together with the scripts that allow it to run, the sprites that compose the ninja, the animation that makes it move, plus others element used by the PlayerPrefab. 

![](./images/PlayerFiles.png)



#### Googles Play Integration and Advertising

 Ads and Google play integration are not hard to understand, they are oneliner. But the main difficulty come from the configuration of those API.







## Scrum

### Demo 1

#### Objectives

For the end of the first iteration our expectation is to finish most of the gameplay of the game. 

We want the player to be able to test a game with most of its functionnalities and see his/her score, altogether with the first shot at the graphism and music.

- A player can control a ninja by swiping the screen (jump and duck)
- The ninja faces obsacles and enemies that are swipables
- The ninja is followed by a deadly entity that catch him up 
- The ninja can take boosts to run faster and awoid slowers that slow him down
- The player's score is shown on the screen
- The player can follow a tutorial to learn the different movements in game
- The game has music and sounds

On the other hand we want to have the Discord channel ready to welcome its first community members, the website to be up and the twitter account open.

- Website up 
- Discord channel created
- Discord bots to welcome players implmented
- Twitter account 
- First articles on the website written



We want to create an association in order to simulate the startup

- Create association



Finally we want to have a feedback system ready to recieve feed backs on the game from the community.

- create a user feedbacks system



#### Results

At the end of the first iteration, we have implemented most of the elements we wished to create.

The missing points were the following: 

- a interactive tutorial (we replaced it with a static tutorial)
- the game has a music but no sounds
- The discord bot doesn't work yet, but the implementation has started
- The association is not created, but has a name: Taram



For this iteration we realised the dynamic of the team didn't work well. We didn't give an equal amount of work to each member for the implementation of the game took the most work. The website, discord and twitter have all been created. 

During this iteration we presented our project to the Dean in order to be allowed to present the game to the PolyLan32. The presentation itself was a rich experience.



### Demo 2

#### Objectives

For this iteration, we want to:

-  proceed to the Google Play Integration

- Implementation of unit tests

- Refactoring of the code

- Implementation of an interactive tutorial (left form interation 1)

- Start implmenting changes into the game, based on the PolyLan (and beta tester in the class) feedbacks. 

- implement bosses 

- implement multiple levels, evolving with the distance

- Rework enemies and obstacles generations

  We decided to put aside all the elements surrounding the game to focus on its development and deployment of the Google Play Store. Members of the team who hadn't use Unity during the first iteration have to learn how to work with it.


#### Results

This iteration was special in the way that one member of the team paused his semester and couldn't work anymore. Morever another one got sick and missed most of the iteration. There was only three developers left. Therefore we didn't reach the objectives that we had planned to do. 

- The Google Play Integration was started but didn't work yet
- Same for the unit tests
- The refactoring was almost done, but not yet
- But we succeed in changing the way the obstacles/enemies were generated

We decided to put aside the implementation of an interactive tutorial, of the bosses and of the multiple and  evolving levels. We left the polyLan Feedbacks for the last iteration.



### Demo 3

#### Objectives

For this last iteration we wanted to:

- Finish the Google Play Integration
- Add Advertising system
- Add a store to the game
- Add users statistics using Firebase
- Create unit tests
- change game based on PolyLan feedbacks



#### Results

During the iteration we faced another unexpected event that forced another member to pause his semester. The situation affected the others, and there were finally only two members to finish what was letf to do in the project. 

As the game was already working we decided to put aside the PolyLan feedbacks and focus on:

-  Google Play integrations,
- The integration of advertising 
- The integration of achievement system
- The integration of statisics on the game 
- The implementation of some tests
- The integration of a store in the game

All thoses points were successfully implemented. Only the store could not be integrated to the game for it needed the game to be published on the Google Play Store, and we didn't expect the validation of the game by google to take more than 2 days.



## Notes on the project

This project was interessting and motivating for we were able to create a game. The learning of the Unity Engine didn't took so much time, but what did was all the technologies surrounding it. 

We were all motivated by the project, yet we experienced the same difficulties than in others project with deadlines and organisation.

The unexpected events that happened during the semester didn't help and we had to accept to lower our expectations. 

Fortunately, the PolyLan event made us give our best on the project during the first par of the semested, giving us a finished product (even though it is not as tweaked as we wished it to be)